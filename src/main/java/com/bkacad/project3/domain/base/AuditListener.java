package com.bkacad.project3.domain.base;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.service.ContextService;
import com.bkacad.project3.service.GenIdService;
import com.bkacad.project3.util.BeanUtil;
import org.springframework.stereotype.Service;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

@Service
public class AuditListener {
    @PreUpdate
    @PreRemove
    @PrePersist

    public void setAudit(AuditAuto auditAuto) {
        ContextService contextService = BeanUtil.getBean(ContextService.class);
        User user = contextService.getCurrentUser();
        if (auditAuto.getId() == null || auditAuto.getId() == 0) {
            if (user != null) {
                auditAuto.setCreatedBy(user.getId());
                auditAuto.setUpdatedBy(user.getId());
                auditAuto.setCreatedByName(user.getFullName());
                auditAuto.setUpdatedByName(user.getFullName());
            }
            LocalDateTime now = LocalDateTime.now();
            GenIdService genIdService = BeanUtil.getBean(GenIdService.class);
            auditAuto.setId(genIdService.nextId());
            auditAuto.setCreatedAt(now);
            auditAuto.setUpdatedAt(now);
            auditAuto.setIsDeleted(false);
        } else {
            if (user != null) {
                auditAuto.setUpdatedBy(contextService.getCurrentUser().getId());
                auditAuto.setUpdatedByName(contextService.getCurrentUser().getFullName());
            }
            auditAuto.setUpdatedAt(LocalDateTime.now());


        }
    }
}
