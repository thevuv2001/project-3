package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.TIME_SLOT)
@Table(name = EntityName.TIME_SLOT)
public class TimeSlot extends AbstractBaseEntity {
    @Column(name = "time_start")
    private LocalTime timeStart;

    @Column(name = "time_end")
    private LocalTime timeEnd;

    @Column(name = "is_required")
    private Boolean isRequired;
}
