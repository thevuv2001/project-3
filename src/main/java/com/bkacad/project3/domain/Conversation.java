package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.CONVERSATION)
@Table(name = EntityName.CONVERSATION)
public class Conversation extends AbstractBaseEntity {
    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "supporter_id")
    private Long supporterId;

    @Column(name = "last_message")
    private String lastMessage;

    @Column(name = "customer_unread")
    private Long customerUnread;

    @Column(name = "supporter_unread")
    private Long supporterUnread;
}
