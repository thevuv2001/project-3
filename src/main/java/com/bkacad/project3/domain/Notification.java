package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.NOTIFICATION)
@Table(name = EntityName.NOTIFICATION)
public class Notification extends AbstractBaseEntity {
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "title")
    private String title;

    @Column(name = "target_id")
    private Long targetId;

    @Column(name = "target_type")
    private Short targetType;

    @Column(name = "action_type")
    private Short actionType;

    @Column(name = "seen")
    private Boolean seen;
}
