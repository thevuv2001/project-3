package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.WORK_SCHEDULE)
@Table(name = EntityName.WORK_SCHEDULE)
public class WorkSchedule extends AbstractBaseEntity {
    @Column(name = "staff_id")
    private Long staffId;

    @Column(name = "time_slot_id_start")
    private Long timeSlotIdStart;

    @Column(name = "time_slot_id_end")
    private Long timeSlotIdEnd;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "type")
    private Short type;
}
