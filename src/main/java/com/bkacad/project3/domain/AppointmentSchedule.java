package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.APPOINTMENT_SCHEDULE)
@Table(name = EntityName.APPOINTMENT_SCHEDULE)
public class AppointmentSchedule extends AbstractBaseEntity {
    @Column(name = "doctor_id")
    private Long doctorId;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "time_slot_id")
    private Long timeSlotId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "status")
    private Short status;

    @Column(name = "note")
    private String note;

    @Column(name = "user_star_rate")
    private Short userStarRate;

    @Column(name = "user_content_rate")
    private String userContentRate;
}
