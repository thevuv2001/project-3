package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.NOTIFICATION_TOKEN)
@Table(name = EntityName.NOTIFICATION_TOKEN)
public class NotificationToken extends AbstractBaseEntity {
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "token")
    private Long token;
}
