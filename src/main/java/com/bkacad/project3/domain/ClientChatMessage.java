package com.bkacad.project3.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ClientChatMessage {
    private Long id;
    private String content;
    private Long senderId;
    private String senderName;
    private Long conversationId;
    private MessageType type;
    private LocalDateTime createdAt;

    public enum MessageType {
        CHAT_TEXT,
        CHAT_IMAGE,
        CHAT_FILE,
        CHAT_VIDEO,
        LEAVE,
        JOIN,
        REGISTER,
        INPUT_START,
        INPUT_END
    }
}
