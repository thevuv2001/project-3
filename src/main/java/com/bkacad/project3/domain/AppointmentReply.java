package com.bkacad.project3.domain;

import com.bkacad.project3.constant.EntityName;
import com.bkacad.project3.domain.base.AbstractBaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = EntityName.APPOINTMENT_REPLY)
@Table(name = EntityName.APPOINTMENT_REPLY)
public class AppointmentReply extends AbstractBaseEntity {
    @Column(name = "appointment_id")
    private Long appointmentId;

    @Column(name = "admin_id")
    private Long adminId;

    @Column(name = "content")
    private String content;
}
