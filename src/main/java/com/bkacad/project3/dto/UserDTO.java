package com.bkacad.project3.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {
    @JMap
    private Long id;

    @JMap
    private String username;

    @JMap
    private String email;

    @JMap
    private String avatar;

    @JMap
    private String fullName;

    @JMap
    private String phone;

    @JMap
    private Boolean gender;

    @JMap
    private Boolean isLock;

    @JMap
    private Short role;

    @JMap
    private Long specialtyId;

    private String specialtyName;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private LocalDateTime updatedAt;
}
