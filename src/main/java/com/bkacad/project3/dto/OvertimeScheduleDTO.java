package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OvertimeScheduleDTO {
    private Long staffId;

    private UserDTO staff;

    private LocalDate date;

    private Long timeSlotIdStart1;

    private TimeSlotDTO timeSlotDTOStart1;

    private Long timeSlotIdEnd1;

    private TimeSlotDTO timeSlotDTOEnd1;

    private Long totalTime1;

    private Long timeSlotIdStart2;

    private TimeSlotDTO timeSlotDTOStart2;

    private Long timeSlotIdEnd2;

    private TimeSlotDTO timeSlotDTOEnd2;

    private Long totalTime2;
}
