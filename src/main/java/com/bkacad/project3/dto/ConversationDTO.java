package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConversationDTO {
    @JMap
    private Long id;

    @JMap
    private Long customerId;

    private UserDTO customer;

    @JMap
    private Long supporterId;

    @JMap
    private String lastMessage;

    @JMap
    private Long customerUnread;

    @JMap
    private Long supporterUnread;

    @JMap
    private Boolean isDeleted;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private Long createdBy;

    @JMap
    private String createdByName;

    @JMap
    private LocalDateTime updatedAt;

    @JMap
    private Long updatedBy;

    @JMap
    private String updatedByName;
}
