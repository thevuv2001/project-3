package com.bkacad.project3.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentCountDTO {
    Long all;
    Long complete;
    Long active;
    Long editable;
    Long waiting;
    Long cancel;
}
