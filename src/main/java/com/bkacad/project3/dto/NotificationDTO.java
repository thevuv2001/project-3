package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDTO {
    @JMap
    private Long id;

    @JMap
    private Long userId;

    @JMap
    private String title;

    @JMap
    private Long targetId;

    @JMap
    private Short targetType;

    @JMap
    private Short actionType;

    @JMap
    private Boolean seen;

    @JMap
    private Boolean isDeleted;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private Long createdBy;

    @JMap
    private String createdByName;

    @JMap
    private LocalDateTime updatedAt;

    @JMap
    private Long updatedBy;

    @JMap
    private String updatedByName;
}
