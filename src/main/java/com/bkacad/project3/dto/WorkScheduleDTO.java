package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkScheduleDTO {
    @JMap
    private Long id;

    @JMap
    private Long staffId;

    private UserDTO staff;

    @JMap
    private Long timeSlotIdStart;

    private TimeSlotDTO timeSlotDTOStart;

    @JMap
    private Long timeSlotIdEnd;

    private TimeSlotDTO timeSlotDTOEnd;

    private Long totalTime;

    @JMap
    private LocalDate date;

    @JMap
    private Short type;

    @JMap
    private Boolean isDeleted;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private Long createdBy;

    @JMap
    private String createdByName;

    @JMap
    private LocalDateTime updatedAt;

    @JMap
    private Long updatedBy;

    @JMap
    private String updatedByName;
}
