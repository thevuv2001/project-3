package com.bkacad.project3.dto;

public interface ICustomerAppointmentRank {
    Long getId();
    String getFull_name();
    Long getCount();
}
