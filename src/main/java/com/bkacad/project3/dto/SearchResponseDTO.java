package com.bkacad.project3.dto;

import lombok.Data;

@Data
public class SearchResponseDTO {
    private Object data;
    private int totalPages;
    private int pageIndex;
    private long totalRecords;
}
