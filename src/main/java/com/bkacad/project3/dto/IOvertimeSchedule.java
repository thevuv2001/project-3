package com.bkacad.project3.dto;

import java.time.LocalDate;

public interface IOvertimeSchedule {
    LocalDate getDate();
    Long getStaff_id();
    Long getTime_slot_id_start_1();
    Long getTime_slot_id_start_2();
    Long getTime_slot_id_end_1();
    Long getTime_slot_id_end_2();
}
