package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TimeSlotDTO {
    @JMap
    private Long id;

    @JMap
    private LocalTime timeStart;

    @JMap
    private LocalTime timeEnd;

    @JMap
    private Boolean isRequired;

    @JMap
    private Boolean isDeleted;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private Long createdBy;

    @JMap
    private String createdByName;

    @JMap
    private LocalDateTime updatedAt;

    @JMap
    private Long updatedBy;

    @JMap
    private String updatedByName;

    private Boolean canRegister;
}
