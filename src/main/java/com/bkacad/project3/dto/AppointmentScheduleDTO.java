package com.bkacad.project3.dto;

import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentScheduleDTO {
    @JMap
    private Long id;

    @JMap
    private Long doctorId;

    private UserDTO doctor;

    @JMap
    private Long customerId;

    private UserDTO customer;

    @JMap
    private Long timeSlotId;

    private TimeSlotDTO timeSlot;

    @JMap
    private LocalDate date;

    @JMap
    private Short status;

    private String timeEnd;

    @JMap
    private String note;

    @JMap
    private Short userStarRate;

    @JMap
    private String userContentRate;

    @JMap
    private Boolean isDeleted;

    @JMap
    private LocalDateTime createdAt;

    @JMap
    private Long createdBy;

    @JMap
    private String createdByName;

    @JMap
    private LocalDateTime updatedAt;

    @JMap
    private Long updatedBy;

    @JMap
    private String updatedByName;
}
