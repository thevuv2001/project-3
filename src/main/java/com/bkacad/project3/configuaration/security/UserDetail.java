package com.bkacad.project3.configuaration.security;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetail implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsernameAndIsLock(username, false);
        if (user.isPresent()) {
            return user.get();
        }
        throw new UsernameNotFoundException("Tài khoản '"+username+"' không tồn tại!");
    }
}
