package com.bkacad.project3.util;

import com.bkacad.project3.dto.SearchResponseDTO;

public class SearchUtil {
    public static <T, R> SearchResponseDTO prepareResponseForSearch(int totalPage, int pageIndex, long totalElement, Object data) {
        SearchResponseDTO response = new SearchResponseDTO();
        response.setData(data);
        response.setPageIndex(pageIndex);
        response.setTotalPages(totalPage);
        response.setTotalRecords(totalElement);
        return response;
    }
}
