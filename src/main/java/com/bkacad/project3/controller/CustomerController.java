package com.bkacad.project3.controller;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.service.ContextService;
import com.bkacad.project3.service.SpecialtyService;
import com.bkacad.project3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/khach-hang")
public class CustomerController {
    @Autowired
    UserService userService;
    @Autowired
    SpecialtyService specialtyService;
    @Autowired
    ContextService contextService;

    @GetMapping("/trang-ca-nhan")
    public String personalPage(Model model){
        return "customer/trang-ca-nhan";
    }

    @PostMapping("/trang-ca-nhan")
    public String updateInfo(
            @RequestParam("name") String name,
            @RequestParam("phone") String phone,
            @RequestParam("avatar") String avatar,
            @RequestParam("gender") Boolean gender,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        User user = contextService.getCurrentUser();
        Boolean check = userService.updateInfo(user.getId(), name, avatar, phone, gender);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
