package com.bkacad.project3.controller;

import com.bkacad.project3.service.FileStorageService;
import com.bkacad.project3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class PublicController{
    @Autowired
    UserService userService;
    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/")
    public String home(){
        return "public/home";
    }

    @GetMapping("/dang-nhap")
    public String login(){
        return "public/login";
    }

    @GetMapping("/dang-ky")
    public String register(){
        return "public/register";
    }

    @PostMapping("/dang-ky")
    public String userRegister(
            @RequestParam("fullName") String fullName,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            RedirectAttributes redirectAttributes
    ){
        Boolean check = userService.signup(fullName, username, password);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_signup_success", true);
            return "redirect:/dang-nhap";
        }else{
            redirectAttributes.addFlashAttribute("toastr_signup_failed", true);
            return "redirect:/dang-ky";
        }
    }

    @PostMapping("/file/upload")
    @ResponseBody
    public String upload(@RequestParam("upload") MultipartFile file) {
        return fileStorageService.storeMultipartFile(file);
    }

    @GetMapping("/file/view/{fileName:.+}")
    @ResponseBody
    public ResponseEntity<Resource> view(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
