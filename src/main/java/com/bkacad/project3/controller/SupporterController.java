package com.bkacad.project3.controller;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.service.ContextService;
import com.bkacad.project3.service.ConversationService;
import com.bkacad.project3.service.MessageService;
import com.bkacad.project3.service.UserService;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/cham-soc-khach-hang")
public class SupporterController {
    @Autowired
    ConversationService conversationService;
    @Autowired
    MessageService messageService;
    @Autowired
    UserService userService;
    @Autowired
    ContextService contextService;

    @GetMapping({"", "/trang-chinh"})
    public String supporterDashboard(){
        return "supporter/dashboard";
    }

    @GetMapping("ho-tro")
    public String conversationList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "search", required = false) String search,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = conversationService.findBySupporter(search, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("conversationList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        return "supporter/ho-tro";
    }

    @GetMapping("ho-tro/{id}")
    public String chat(
            @PathVariable(name = "id") Long id,
            Model model
    ){
        ConversationDTO conversationDTO = conversationService.findById(id);
        UserDTO userDTO = userService.findById(conversationDTO.getCustomerId());
        User user = contextService.getCurrentUser();
        List<MessageDTO> messageDTOList = messageService.getMessageByConversation(conversationDTO.getId());
        model.addAttribute("userId", user.getId());
        model.addAttribute("conversationId", conversationDTO.getId());
        model.addAttribute("customer", userDTO);
        model.addAttribute("messageList", messageDTOList);
        return "supporter/chat";
    }
}
