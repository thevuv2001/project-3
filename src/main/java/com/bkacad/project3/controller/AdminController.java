package com.bkacad.project3.controller;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.DoughnutChartDTO;
import com.bkacad.project3.dto.StatisticsDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/quan-tri-vien")
public class AdminController {
    @Autowired
    AppointmentScheduleService appointmentScheduleService;
    @Autowired
    UserService userService;
    @Autowired
    SpecialtyService specialtyService;
    @Autowired
    ContextService contextService;

    @GetMapping({"", "/trang-chinh"})
    public String adminDashboard(
            Model model,
            @RequestParam(value = "thang", required = false) Integer thang
    ){
        StatisticsDTO appointmentStatics = appointmentScheduleService.statistics();
        StatisticsDTO customerStatics = userService.statistics(Arrays.asList((short) 4));
        StatisticsDTO doctorStatics = userService.statistics(Arrays.asList((short) 2));
        StatisticsDTO specialtyStatics = specialtyService.statistics();
        List<Long> yearNewUserChart = userService.yearNewUserChart();
        DoughnutChartDTO specialtyChart = specialtyService.doughnutChart();
        Long maxStaffCount = userService.maxStaffCount();
        List<Long> yearAllAppointmentChart = appointmentScheduleService.yearNewAppointmentChart(Arrays.asList((short) 6, (short) 8, (short) 9, (short) 10));
        List<Long> yearActiveAppointmentChart = appointmentScheduleService.yearNewAppointmentChart(Arrays.asList((short) 6));
        List<Long> yearCompleteAppointmentChart = appointmentScheduleService.yearNewAppointmentChart(Arrays.asList((short) 9, (short) 10));
        List<Long> yearRejectAppointmentChart = appointmentScheduleService.yearNewAppointmentChart(Arrays.asList((short) 8));
        List<Long> monthAllAppointmentChart = appointmentScheduleService.monthNewAppointmentChart(Arrays.asList((short) 6, (short) 8, (short) 9, (short) 10), thang);
        List<Long> monthActiveAppointmentChart = appointmentScheduleService.monthNewAppointmentChart(Arrays.asList((short) 6), thang);
        List<Long> monthCompleteAppointmentChart = appointmentScheduleService.monthNewAppointmentChart(Arrays.asList((short) 9, (short) 10), thang);
        List<Long> monthRejectAppointmentChart = appointmentScheduleService.monthNewAppointmentChart(Arrays.asList((short) 8), thang);
        List<Integer> dayList = appointmentScheduleService.dayList();
        if(thang == null || thang < 1 || thang > 12){
            LocalDate currentDate = LocalDate.now();
            thang = currentDate.getMonthValue();
        }
        List<Long> statusChart = appointmentScheduleService.statusList();
        DoughnutChartDTO doctorRanking = appointmentScheduleService.doctorRanking();
        model.addAttribute("appointmentStatics", appointmentStatics);
        model.addAttribute("customerStatics", customerStatics);
        model.addAttribute("doctorStatics", doctorStatics);
        model.addAttribute("specialtyStatics", specialtyStatics);
        model.addAttribute("yearNewUserChart", yearNewUserChart);
        model.addAttribute("specialtyChart", specialtyChart);
        model.addAttribute("maxStaffCount", maxStaffCount);
        model.addAttribute("yearAllAppointmentChart", yearAllAppointmentChart);
        model.addAttribute("yearActiveAppointmentChart", yearActiveAppointmentChart);
        model.addAttribute("yearCompleteAppointmentChart", yearCompleteAppointmentChart);
        model.addAttribute("yearRejectAppointmentChart", yearRejectAppointmentChart);
        model.addAttribute("monthAllAppointmentChart", monthAllAppointmentChart);
        model.addAttribute("monthActiveAppointmentChart", monthActiveAppointmentChart);
        model.addAttribute("monthCompleteAppointmentChart", monthCompleteAppointmentChart);
        model.addAttribute("monthRejectAppointmentChart", monthRejectAppointmentChart);
        model.addAttribute("dayList", dayList);
        model.addAttribute("month", thang);
        model.addAttribute("statusChart", statusChart);
        model.addAttribute("doctorRanking", doctorRanking);
        return "admin/trang-chinh";
    }

    @GetMapping("/them-lich-hen")
    public String appointmentInsert(){
        return "admin/them-lich-hen";
    }

    @GetMapping("/danh-sach-danh-gia")
    public String ratingList(){
        return "admin/danh-sach-danh-gia";
    }

    @GetMapping("/trang-ca-nhan")
    public String personalPage(Model model){
        return "admin/trang-ca-nhan";
    }

    @PostMapping("/trang-ca-nhan")
    public String updateInfo(
            @RequestParam("name") String name,
            @RequestParam("phone") String phone,
            @RequestParam("avatar") String avatar,
            @RequestParam("gender") Boolean gender,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        User user = contextService.getCurrentUser();
        Boolean check = userService.updateInfo(user.getId(), name, avatar, phone, gender);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
