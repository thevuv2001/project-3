package com.bkacad.project3.controller.admin;

import com.bkacad.project3.constant.AppointmentStatus;
import com.bkacad.project3.constant.ScheduleType;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.service.AppointmentScheduleService;
import com.bkacad.project3.service.TimeSlotService;
import com.bkacad.project3.service.UserService;
import com.bkacad.project3.service.WorkScheduleService;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/quan-tri-vien/lich-trinh")
public class AdminScheduleController {
    @Autowired
    TimeSlotService timeSlotService;
    @Autowired
    AppointmentScheduleService appointmentScheduleService;
    @Autowired
    UserService userService;
    @Autowired
    WorkScheduleService workScheduleService;

    @GetMapping({"{stringOfdate}",""})
    public String adminCalendar(
            @PathVariable(required=false) Optional<String> stringOfdate,
            Model model
    ){
        int month;
        int year;
        if (stringOfdate.isPresent()){
            if(stringOfdate.get().matches("^thang-([1-9]|1[0-2])-nam-20[0-9][0-9]$")){
                String[] parts = stringOfdate.get().split("-");
                month = Integer.parseInt(parts[1]);
                year = Integer.parseInt(parts[3]);
            }else{
                month = LocalDate.now().getMonthValue();
                year = LocalDate.now().getYear();
            }
        }else{
            month = LocalDate.now().getMonthValue();
            year = LocalDate.now().getYear();
        }
        LocalDate currentdate = LocalDate.now();
        LocalDate firstDate = LocalDate.of(year, month, 1);
        model.addAttribute("currentDate", currentdate);
        YearMonth ym = YearMonth.of(year,month);
        LocalDate firstOfMonth = ym.atDay(1);
        LocalDate firstOfFollowingMonth = ym.plusMonths(1).atDay(1);
        long numOfDaysBetween = ChronoUnit.DAYS.between(firstOfMonth, firstOfFollowingMonth);
        List<LocalDate> dayList = IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> firstOfMonth.plusDays(i))
                .collect(Collectors.toList());
        List<Integer> previousDayList = new ArrayList<>();
        for(int i = 1; i < firstDate.getDayOfWeek().getValue(); i++){
            previousDayList.add(i);
        }
        model.addAttribute("month", ym);
        model.addAttribute("currentDate", currentdate);
        model.addAttribute("firstDate", firstDate);
        model.addAttribute("dayList", dayList);
        model.addAttribute("previousDayList", previousDayList);
        return "admin/lich-trinh";
    }

    @PostMapping("{stringOfdate}")
    public String adminChangeCalendar(
            @PathVariable String stringOfdate
    ){
        return "redirect:/quan-tri-vien/lich-trinh/"+ stringOfdate;
    }

    @GetMapping({"/danh-sach-lich-hen"})
    public String appointmentList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "status", required = false) Short status,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        AppointmentCountDTO appointmentCountDTO = appointmentScheduleService.getAppointmentCountForAdmin();
        SearchResponseDTO searchResponseDTO = appointmentScheduleService.findForAdmin(pageDTO.getCurrentPage() - 1, pageDTO.getPageSize(), status);
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("count", appointmentCountDTO);
        model.addAttribute("timeSlotList",timeSlotService.findAll());
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("status", status);
        return "admin/danh-sach-lich-hen";
    }

    @GetMapping("/danh-sach-lich-hen/chi-tiet/{id}")
    public String appointmentDetail(
            @PathVariable Long id,
            Model model
    ){
        AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.findByIdForAdmin(id);
        if(appointmentScheduleDTO.getDoctorId() != null){
            List<AppointmentScheduleDTO> appointmentScheduleDTOList = appointmentScheduleService.findByDoctorIdAndDate(appointmentScheduleDTO.getDoctorId(), appointmentScheduleDTO.getDate(), appointmentScheduleDTO.getTimeSlotId(), id);
            model.addAttribute("doctorAppointment", appointmentScheduleDTOList);
            Boolean conflict = appointmentScheduleService.checkConflictAppointment(appointmentScheduleDTO.getTimeSlotId(), appointmentScheduleDTO.getDate(), appointmentScheduleDTO.getDoctorId(), id);
            model.addAttribute("conflict", conflict);
        }
        model.addAttribute("schedule", appointmentScheduleDTO);
        model.addAttribute("id", id);
        return "admin/chi-tiet-lich-hen";
    }

    @PostMapping("/danh-sach-lich-hen/chi-tiet/{id}/duyet")
    public String acceptAppointment(
            @PathVariable Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = appointmentScheduleService.approveAppointment(id, AppointmentStatus.ACCEPT.value, null);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Duyệt thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Duyệt thất bại");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;

    }

    @PostMapping("/danh-sach-lich-hen/chi-tiet/{id}/tu-choi")
    public String rejectAppointment(
            @PathVariable Long id,
            @RequestParam(value = "content") String content,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = appointmentScheduleService.approveAppointment(id, AppointmentStatus.REJECT.value, content);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Từ chối thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Từ chối thất bại");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;

    }

    @PostMapping("/danh-sach-lich-hen/chi-tiet/{id}/tra-ve")
    public String resetAppointment(
            @PathVariable Long id,
            @RequestParam(value = "content") String content,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = appointmentScheduleService.approveAppointment(id, AppointmentStatus.RESET.value, content);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Trả về thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Trả về thất bại");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;

    }

    @GetMapping({"/danh-sach-lich-nghi"})
    public String holidayScheduleList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = workScheduleService.findHolidayScheduleForAdmin(date,pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("date", date);
        return "admin/danh-sach-lich-nghi";
    }

    @GetMapping({"/danh-sach-lich-tang-ca"})
    public String overtimeScheduleList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = workScheduleService.findOvertimeScheduleForAdmin(date,pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("date", date);
        return "admin/danh-sach-lich-tang-ca";
    }
}
