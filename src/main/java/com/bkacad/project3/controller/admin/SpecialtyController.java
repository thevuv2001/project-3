package com.bkacad.project3.controller.admin;

import com.bkacad.project3.constant.AppointmentStatus;
import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.dto.PageDTO;
import com.bkacad.project3.dto.SearchResponseDTO;
import com.bkacad.project3.dto.SpecialtyDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.service.AppointmentScheduleService;
import com.bkacad.project3.service.SpecialtyService;
import com.bkacad.project3.service.UserService;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/quan-tri-vien/danh-sach-chuyen-khoa")
public class SpecialtyController {
    @Autowired
    SpecialtyService specialtyService;
    @Autowired
    UserService userService;
    @Autowired
    AppointmentScheduleService appointmentScheduleService;

    @GetMapping("")
    public String specialtyList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "search", required = false) String search,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = specialtyService.search(search, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        List<UserDTO> userDTOList = userService.findDoctorDontHaveSpecialty();
        model.addAttribute("specialtyList",searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("search", search);
        model.addAttribute("noSpecialtyDoctorList", userDTOList);
        return "admin/danh-sach-chuyen-khoa";
    }

    @PostMapping("")
    public String insertSpecialty(
            @RequestParam("name") String name,
            @RequestParam(value = "doctor", required = false) List<Long> doctorIdList,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = specialtyService.create(name, doctorIdList);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @GetMapping("chi-tiet/{id}")
    public String specialtyDetail(
            @PathVariable(name = "id") Long id,
            Model model){
        SpecialtyDTO specialtyDTO = specialtyService.findById(id);
        List<UserDTO> userDTOList = userService.findAllByRole(ROLE.DOCTOR.value);
        List<UserDTO> userDTOListBySpecialty = userService.findBySpecialtyId(id);
        Long totalCount = appointmentScheduleService.countBySpecialtyId(id);
        Double countComplete = appointmentScheduleService.getPercentByStatusAndSpecialty(Arrays.asList(AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), id);
        Double countReject = appointmentScheduleService.getPercentByStatusAndSpecialty(Arrays.asList(AppointmentStatus.REJECT.value), id);
        model.addAttribute("doctorSearchList", userDTOList);
        model.addAttribute("specialty",specialtyDTO);
        model.addAttribute("doctorList", userDTOListBySpecialty);
        model.addAttribute("totalCount", totalCount);
        model.addAttribute("countComplete", countComplete);
        model.addAttribute("countReject", countReject);
        return "admin/chi-tiet-chuyen-khoa";
    }

    @PostMapping("chi-tiet/{id}/update-doctor")
    public String specialtyUpdateDoctor(
            @PathVariable(name = "id") Long id,
            @RequestParam(name = "doctorId") List<Long> doctorIdList,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = specialtyService.updateDoctor(id, doctorIdList);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_update_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_update_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("chi-tiet/{id}/update-name")
    public String specialtyUpdateName(
            @PathVariable(name = "id") Long id,
            @RequestParam(name = "name") String name,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = specialtyService.updateName(id, name);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_update_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_update_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
