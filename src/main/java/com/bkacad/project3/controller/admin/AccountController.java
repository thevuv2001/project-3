package com.bkacad.project3.controller.admin;

import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.dto.PageDTO;
import com.bkacad.project3.dto.SearchResponseDTO;
import com.bkacad.project3.dto.SpecialtyDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.service.SpecialtyService;
import com.bkacad.project3.service.UserService;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/quan-tri-vien/quan-tri-tai-khoan")
public class AccountController {
    @Autowired
    UserService userService;
    @Autowired
    SpecialtyService specialtyService;

    @GetMapping("/danh-sach-khach-hang")
    public String customerList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "search", required = false) String search,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = userService.searchByRole(ROLE.CUSTOMER.value, search, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("accountList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("search", search);
        return "admin/danh-sach-khach-hang";
    }

    @GetMapping("/danh-sach-bac-si")
    public String doctorList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "search", required = false) String search,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = userService.searchDoctor(search, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        List<SpecialtyDTO> specialtyDTOList = specialtyService.findAll();
        model.addAttribute("accountList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("search", search);
        model.addAttribute("specialtyList", specialtyDTOList);
        return "admin/danh-sach-bac-si";
    }

    @GetMapping("/danh-sach-cham-soc-khach-hang")
    public String supporterList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "search", required = false) String search,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = userService.searchByRole(ROLE.SUPPORTER.value, search, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("accountList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("search", search);
        return "admin/danh-sach-cham-soc-khach-hang";
    }

    @PostMapping("/them-tai-khoan")
    public String userCreate(
            @RequestParam("fullName") String fullName,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("role") Short role,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = userService.userCreate(fullName, username, password, role);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("/them-bac-si")
    public String doctorCreate(
            @RequestParam("fullName") String fullName,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("role") Short role,
            @RequestParam(value = "specialty", required = false) Long specialty,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = userService.doctorCreate(fullName, username, password, role, specialty);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("/khoa-tai-khoan/{id}")
    public String userLock(
            @PathVariable(name = "id") Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = userService.adminLock(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Khoa");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("/mo-khoa-tai-khoan/{id}")
    public String userUnlock(
            @PathVariable(name = "id") Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = userService.adminUnlock(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
