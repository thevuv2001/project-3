package com.bkacad.project3.controller.doctor;

import com.bkacad.project3.constant.ScheduleType;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.service.AppointmentScheduleService;
import com.bkacad.project3.service.TimeSlotService;
import com.bkacad.project3.service.WorkScheduleService;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/bac-si/lich-trinh")
public class DoctorScheduleController {
    @Autowired
    WorkScheduleService workScheduleService;
    @Autowired
    TimeSlotService timeSlotService;
    @Autowired
    AppointmentScheduleService appointmentScheduleService;

    @GetMapping("lich-nghi")
    public String holidaySchedule(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = workScheduleService.findByStaffAndType(date, ScheduleType.HOLIDAY.value,pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIsRequired(true);
        List<WorkScheduleDTO> workScheduleDTOList = workScheduleService.todayDoctorSchedule(ScheduleType.HOLIDAY.value);
        LocalDate currentdate = LocalDate.now();
        model.addAttribute("currentDate", currentdate);
        model.addAttribute("todayScheduleList", workScheduleDTOList);
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("workTimeSlotList", timeSlotDTOList);
        model.addAttribute("minTime", timeSlotDTOList.get(0).getId());
        model.addAttribute("maxTime", timeSlotDTOList.get(timeSlotDTOList.size() - 1).getId());
        return "doctor/lich-nghi";
    }

    @PostMapping("lich-nghi")
    public String createHolidaySchedule(
            @RequestParam(value = "timeSlotStart") Long timeSlotStart,
            @RequestParam(value = "timeSlotEnd") Long timeSlotEnd,
            @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.createHolidaySchedule(timeSlotStart, timeSlotEnd, date);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Mỗi ngày chỉ có thể tạo 1 lịch nghỉ");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("lich-nghi/update")
    public String updateHolidaySchedule(
            @RequestParam(value = "timeSlotStart") Long timeSlotStart,
            @RequestParam(value = "timeSlotEnd") Long timeSlotEnd,
            @RequestParam(value = "id") Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.updateWorkSchedule(id,timeSlotStart, timeSlotEnd, ScheduleType.HOLIDAY.value);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_update_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_update_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("lich-nghi/delete/{id}")
    public String deleteHolidaySchedule(
            @PathVariable(name = "id") Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.deleteWorkSchedule(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_delete_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_delete_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @GetMapping("lich-tang-ca")
    public String overtimeSchedule(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = workScheduleService.findOvertimeScheduleByStaffId(date, pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        List<TimeSlotDTO> timeSlotDTOMorningList = timeSlotService.findByShift(true);
        List<TimeSlotDTO> timeSlotDTONightList = timeSlotService.findByShift(false);
        List<OvertimeScheduleDTO> workScheduleDTOList = workScheduleService.todayDoctorOvertimeSchedule(ScheduleType.OVERTIME.value);
        LocalDate currentdate = LocalDate.now();
        model.addAttribute("currentDate", currentdate);
        model.addAttribute("todayScheduleList", workScheduleDTOList);
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("morningWorkTimeSlotList", timeSlotDTOMorningList);
        model.addAttribute("morningMinTime", timeSlotDTOMorningList.get(0).getId());
        model.addAttribute("morningMaxTime", timeSlotDTOMorningList.get(timeSlotDTOMorningList.size() - 1).getId());
        model.addAttribute("nightWorkTimeSlotList", timeSlotDTONightList);
        model.addAttribute("nightMinTime", timeSlotDTONightList.get(0).getId());
        model.addAttribute("nightMaxTime", timeSlotDTONightList.get(timeSlotDTONightList.size() - 1).getId());
        return "doctor/lich-tang-ca";
    }

    @PostMapping("lich-tang-ca")
    public String createOvertimeSchedule(
            @RequestParam(value = "morningShift", required = false) Boolean morningShift,
            @RequestParam(value = "morningTimeSlotStart") Long morningTimeSlotStart,
            @RequestParam(value = "morningTimeSlotEnd") Long morningTimeSlotEnd,
            @RequestParam(value = "nightShift", required = false) Boolean nightShift,
            @RequestParam(value = "nightTimeSlotStart") Long nightTimeSlotStart,
            @RequestParam(value = "nightTimeSlotEnd") Long nightTimeSlotEnd,
            @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.createOvertimeSchedule(morningShift, morningTimeSlotStart, morningTimeSlotEnd, nightShift, nightTimeSlotStart, nightTimeSlotEnd, date);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Mỗi ca trong ngày chỉ có thể tạo 1 lịch tăng ca");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("lich-tang-ca/update")
    public String updateOvertimeSchedule(
            @RequestParam(value = "morningShift", required = false) Boolean morningShift,
            @RequestParam(value = "morningTimeSlotStart") Long morningTimeSlotStart,
            @RequestParam(value = "morningTimeSlotEnd") Long morningTimeSlotEnd,
            @RequestParam(value = "nightShift", required = false) Boolean nightShift,
            @RequestParam(value = "nightTimeSlotStart") Long nightTimeSlotStart,
            @RequestParam(value = "nightTimeSlotEnd") Long nightTimeSlotEnd,
            @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.updateOvertimeSchedule(morningShift, morningTimeSlotStart, morningTimeSlotEnd, nightShift, nightTimeSlotStart, nightTimeSlotEnd, date);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_update_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Mỗi ca trong ngày chỉ có thể tạo 1 lịch tăng ca");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("lich-tang-ca/delete")
    public String deleteOvertimeSchedule(
            @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = workScheduleService.deleteOvertimeSchedule(date);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_delete_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_delete_failed", true);
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @GetMapping("lich-hen")
    public String appointmentList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            Model model
    ){
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = appointmentScheduleService.findForDoctor(date,pageDTO.getCurrentPage() - 1, pageDTO.getPageSize());
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("date", date);
        return "doctor/lich-hen";
    }

    @PostMapping("lich-hen/hoan-thanh/{id}")
    public String completeAppointment(
            @PathVariable Long id,
            RedirectAttributes redirectAttributes
    ){
        Boolean check = appointmentScheduleService.completeAppointment(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Hoàn thành lịch hẹn thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Hoàn thành lịch hẹn thất bại");
        }
        return  "redirect:/bac-si/lich-trinh/lich-hen";
    }
}
