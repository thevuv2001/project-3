package com.bkacad.project3.controller;

import com.bkacad.project3.domain.ClientChatMessage;
import com.bkacad.project3.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {
    @Autowired
    MessageService messageService;
    @Autowired
    private SimpMessagingTemplate template;
    @MessageMapping("/chat/conversation/{id}")
    public void sendMessage(@DestinationVariable Long id, @Payload ClientChatMessage clientChatMessage, SimpMessageHeaderAccessor headerAccessor) {
        clientChatMessage.setConversationId(id);
        clientChatMessage = messageService.sendMessage(clientChatMessage);
        this.template.convertAndSend(
                "/topic/conversation/"+id,
                clientChatMessage, headerAccessor.getMessageHeaders());
    }
}
