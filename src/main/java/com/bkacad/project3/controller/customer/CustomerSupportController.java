package com.bkacad.project3.controller.customer;

import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.ConversationDTO;
import com.bkacad.project3.dto.MessageDTO;
import com.bkacad.project3.service.ContextService;
import com.bkacad.project3.service.ConversationService;
import com.bkacad.project3.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("khach-hang/cham-soc-khach-hang")
public class CustomerSupportController {
    @Autowired
    ContextService contextService;
    @Autowired
    ConversationService conversationService;
    @Autowired
    MessageService messageService;

    @GetMapping("")
    public String support(Model model){
        User user = contextService.getCurrentUser();
        ConversationDTO conversationDTO = conversationService.createConversation();
        List<MessageDTO> messageDTOList = messageService.getMessageByConversation(conversationDTO.getId());
        model.addAttribute("conversationId", conversationDTO.getId());
        model.addAttribute("userId", user.getId());
        model.addAttribute("messageList", messageDTOList);
        return "customer/cham-soc-khach-hang";
    }
}
