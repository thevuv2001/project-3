package com.bkacad.project3.controller.customer;

import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.domain.AppointmentSchedule;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.service.*;
import com.bkacad.project3.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("khach-hang/lich-trinh")
public class CustomerScheduleController {
    @Autowired
    SpecialtyService specialtyService;
    @Autowired
    UserService userService;
    @Autowired
    AppointmentScheduleService appointmentScheduleService;
    @Autowired
    TimeSlotService timeSlotService;
    @Autowired
    AppointmentReplyService appointmentReplyService;

    @GetMapping({"{stringOfdate}",""})
    public String customerCalendar(
            @PathVariable(required=false) Optional<String> stringOfdate,
            Model model
    ){
        int month;
        int year;
        if (stringOfdate.isPresent()){
            if(stringOfdate.get().matches("^thang-([1-9]|1[0-2])-nam-20[0-9][0-9]$")){
                String[] parts = stringOfdate.get().split("-");
                month = Integer.parseInt(parts[1]);
                year = Integer.parseInt(parts[3]);
            }else{
                month = LocalDate.now().getMonthValue();
                year = LocalDate.now().getYear();
            }
        }else{
            month = LocalDate.now().getMonthValue();
            year = LocalDate.now().getYear();
        }
        LocalDate currentdate = LocalDate.now();
        LocalDate firstDate = LocalDate.of(year, month, 1);
        model.addAttribute("currentDate", currentdate);
        YearMonth ym = YearMonth.of(year,month);
        LocalDate firstOfMonth = ym.atDay(1);
        LocalDate firstOfFollowingMonth = ym.plusMonths(1).atDay(1);
        long numOfDaysBetween = ChronoUnit.DAYS.between(firstOfMonth, firstOfFollowingMonth);
        List<LocalDate> dayList = IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(i -> firstOfMonth.plusDays(i))
                .collect(Collectors.toList());
        List<Integer> previousDayList = new ArrayList<>();
        for(int i = 1; i < firstDate.getDayOfWeek().getValue(); i++){
            previousDayList.add(i);
        }
        model.addAttribute("month", ym);
        model.addAttribute("currentDate", currentdate);
        model.addAttribute("firstDate", firstDate);
        model.addAttribute("dayList", dayList);
        model.addAttribute("previousDayList", previousDayList);
        return "customer/lich-trinh";
    }

    @PostMapping("{stringOfdate}")
    public String adminChangeCalendar(
            @PathVariable String stringOfdate
    ){
        return "redirect:/lich-trinh/"+ stringOfdate;
    }

    @GetMapping("danh-sach-lich-hen")
    public String customerAppointmentList(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "status", required = false) Short status,
            Model model
    ){
        AppointmentCountDTO appointmentCountDTO = appointmentScheduleService.getAppointmentCount();
        PageDTO pageDTO = PageUtil.setDefault(page, null);
        SearchResponseDTO searchResponseDTO = appointmentScheduleService.findByCustomerId(pageDTO.getCurrentPage() - 1, pageDTO.getPageSize(), status);
        model.addAttribute("count", appointmentCountDTO);
        model.addAttribute("scheduleList", searchResponseDTO);
        model.addAttribute("page", PageUtil.format(pageDTO.getCurrentPage(),searchResponseDTO.getTotalPages(), pageDTO.getPageSize()));
        model.addAttribute("status", status);
        return  "customer/danh-sach-lich-hen";
    }

    // Form 1: Chọn bác sĩ và ngày
    @GetMapping({"them-lich-hen","them-lich-hen/chon-bac-si", "them-lich-hen/chon-bac-si/{id}"})
    public String createCustomerAppointmentListForm1(
            @PathVariable(required=false) Optional<Long> id,
            Model model
    ){
        List<SpecialtyDTO> specialtyDTOList = specialtyService.findAll();
        List<UserDTO> userDTOList = userService.findAllByRole(ROLE.DOCTOR.value);
        model.addAttribute("specialtyList", specialtyDTOList);
        model.addAttribute("doctorList", userDTOList);
        model.addAttribute("form",1);
        if(id.isPresent()){
            AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.findById(id.get());
            if(appointmentScheduleDTO.getDoctorId() != null){
                UserDTO userDTO = userService.findById(appointmentScheduleDTO.getDoctorId());
                model.addAttribute("doctor", userDTO);
            }
            List<AppointmentReplyDTO> appointmentReplyDTOList = appointmentReplyService.findByAppointmentId(id.get());
            model.addAttribute("date", appointmentScheduleDTO.getDate());
            model.addAttribute("appointmentId", id.get());
            model.addAttribute("appointmentReplyList", appointmentReplyDTOList);
        }
        return  "customer/them-lich-hen";
    }

    // Lưu bác sĩ của lịch hẹn và chuyển sang form 2
    @PostMapping({"them-lich-hen/chon-bac-si", "them-lich-hen/chon-bac-si/{id}"})
    public String createAppointmentChooseDoctor(
            @PathVariable(required=false) Optional<Long> id,
            @RequestParam(value = "doctorId", required = false) Long doctorId,
            @RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date
    ){
        AppointmentScheduleDTO appointmentScheduleDTO;
        if(id.isPresent()){
            appointmentScheduleDTO = appointmentScheduleService.createAppointmentForm1(id.get(), doctorId, date);
        }else{
            appointmentScheduleDTO = appointmentScheduleService.createAppointmentForm1(null, doctorId, date);
        }
        return  "redirect:/khach-hang/lich-trinh/them-lich-hen/chon-khung-gio/" + appointmentScheduleDTO.getId();
    }

    // Form 2: Chọn khung giờ
    @GetMapping("them-lich-hen/chon-khung-gio/{id}")
    public String createCustomerAppointmentListForm2(
            @PathVariable Long id,
            Model model
    ){
        AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.findById(id);
        List<TimeSlotDTO> timeSlotDTOList;
        if(appointmentScheduleDTO.getDoctorId() != null){
            timeSlotDTOList = timeSlotService.findDoctorWorkTimeSlot(appointmentScheduleDTO.getDoctorId(), appointmentScheduleDTO.getDate());
        }else{
            timeSlotDTOList = timeSlotService.findAll();
        }
        if(appointmentScheduleDTO.getTimeSlotId() != null){
            TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentScheduleDTO.getTimeSlotId());
            model.addAttribute("timeSlot", timeSlotDTO);
        }
        List<AppointmentReplyDTO> appointmentReplyDTOList = appointmentReplyService.findByAppointmentId(id);
        model.addAttribute("appointmentReplyList", appointmentReplyDTOList);
        model.addAttribute("timeSlotList", timeSlotDTOList);
        model.addAttribute("form",2);
        model.addAttribute("appointmentId", id);
        return "customer/them-lich-hen";
    }

    // Lưu khung giờ và chuyển sang form 3
    @PostMapping("them-lich-hen/chon-khung-gio/{id}")
    public String createAppointmentChooseTimeSlot(
            @PathVariable Long id,
            @RequestParam(value = "timeSlotId") Long timeSlotId
    ){
        AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.createAppointmentForm2(id, timeSlotId);
        return "redirect:/khach-hang/lich-trinh/them-lich-hen/dien-thong-tin/" + appointmentScheduleDTO.getId();
    }

    // Form 3: Điền thông tin
    @GetMapping("them-lich-hen/dien-thong-tin/{id}")
    public String createCustomerAppointmentListForm3(
            @PathVariable Long id,
            Model model
    ){
        AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.findById(id);
        List<AppointmentReplyDTO> appointmentReplyDTOList = appointmentReplyService.findByAppointmentId(id);
        model.addAttribute("appointmentReplyList", appointmentReplyDTOList);
        model.addAttribute("note", appointmentScheduleDTO.getNote());
        model.addAttribute("appointmentId", id);
        model.addAttribute("form",3);
        return "customer/them-lich-hen";
    }

    // Lưu thông tin lịch hẹn và chuyển sang form 4
    @PostMapping("them-lich-hen/dien-thong-tin/{id}")
    public String createAppointmentWriteInfo(
            @PathVariable Long id,
            @RequestParam(value = "note", required = false) String note
    ){
        AppointmentScheduleDTO appointmentScheduleDTO = appointmentScheduleService.createAppointmentForm3(id, note);
        return  "redirect:/khach-hang/lich-trinh/them-lich-hen/xac-nhan-dieu-khoan/" + appointmentScheduleDTO.getId();
    }

    // Form 4: Xác nhận điều khoản
    @GetMapping("them-lich-hen/xac-nhan-dieu-khoan/{id}")
    public String createCustomerAppointmentListForm4(
            @PathVariable Long id,
            Model model
    ){
        List<AppointmentReplyDTO> appointmentReplyDTOList = appointmentReplyService.findByAppointmentId(id);
        model.addAttribute("appointmentReplyList", appointmentReplyDTOList);
        model.addAttribute("appointmentId", id);
        model.addAttribute("form",4);
        return  "customer/them-lich-hen";
    }

    // Đổi trạng thái sang chờ duyệt và chuyển về trang danh sách
    @PostMapping("them-lich-hen/xac-nhan-dieu-khoan/{id}")
    public String createAppointmentAcceptPolicy(
            @PathVariable Long id,
            RedirectAttributes redirectAttributes
    ){
        Boolean check = appointmentScheduleService.createAppointmentForm4(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_insert_success", true);
        }else{
            redirectAttributes.addFlashAttribute("toastr_insert_failed", true);
        }
        return  "redirect:/khach-hang/lich-trinh/danh-sach-lich-hen";
    }

    @PostMapping("huy-lich-hen/{id}")
    public String cancelAppointment(
            @PathVariable Long id,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = appointmentScheduleService.cancelAppointment(id);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Hủy bỏ lịch hẹn thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Hủy bỏ lịch hẹn thất bại");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @PostMapping("danh-gia-lich-hen")
    public String rateAppointment(
            @RequestParam(value = "id") Long id,
            @RequestParam(value = "star") Short star,
            @RequestParam(value = "content") String content,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request
    ){
        Boolean check = appointmentScheduleService.rateAppointment(id, star, content);
        if(check){
            redirectAttributes.addFlashAttribute("toastr_custom_success", "Đánh giá lịch hẹn thành công");
        }else{
            redirectAttributes.addFlashAttribute("toastr_custom_failed", "Đánh giá lịch hẹn thất bại");
        }
        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }
}
