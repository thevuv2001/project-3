package com.bkacad.project3.controller;

import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.constant.SiteMap;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.NotificationDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.service.ContextService;
import com.bkacad.project3.service.NotificationService;
import com.bkacad.project3.service.UserService;
import com.bkacad.project3.util.BeanUtil;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalControllerAdvice {
    @Autowired
    HttpServletRequest request;
    @Autowired
    NotificationService notificationService;
    @Autowired
    ContextService contextService;
    @Autowired
    UserService userService;

    @ModelAttribute("UserContextInfo")
    public void populateUser() throws Exception {
        ContextService contextService = BeanUtil.getBean(ContextService.class);

        SiteMap siteMap = SiteMap.findUri(request.getRequestURI());
        if (siteMap != null) {
            if(contextService.getCurrentUser() != null){
                List<SiteMap> sameRoleSiteMaps = SiteMap.findSiteMapByRole(ROLE.findByValue(contextService.getCurrentUser().getRole()));
                if (!sameRoleSiteMaps.contains(siteMap)) {
                    throw new AccessDeniedException("Truy cập bị từ chối:" + siteMap.getUri());
                }
            }else{
                List<SiteMap> allSiteMap = new ArrayList<SiteMap>(EnumSet.allOf(SiteMap.class));
                if(allSiteMap.contains(siteMap)){
                    throw new AccessDeniedException("Truy cập bị từ chối:" + siteMap.getUri());
                }
            }
        }
    }

    @ModelAttribute("UserNotification")
    public void notificationUser(Model model){
        User user = contextService.getCurrentUser();
        if(user != null){
            List<NotificationDTO> notificationDTOList = notificationService.getNotificationByUserId();
            model.addAttribute("notificationList", notificationDTOList);
            Boolean unread = notificationService.unread();
            model.addAttribute("unread", unread);
            UserDTO userDTO = userService.findById(user.getId());
            model.addAttribute("user", userDTO);
        }
    }
}
