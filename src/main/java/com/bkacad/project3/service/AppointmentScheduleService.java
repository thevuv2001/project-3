package com.bkacad.project3.service;

import com.bkacad.project3.constant.*;
import com.bkacad.project3.domain.AppointmentSchedule;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.repository.AppointmentScheduleRepository;
import com.bkacad.project3.repository.UserRepository;
import com.bkacad.project3.util.SearchUtil;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppointmentScheduleService {
    @Autowired
    AppointmentScheduleRepository appointmentScheduleRepository;
    @Autowired
    ContextService contextService;
    @Autowired
    UserService userService;
    @Autowired
    TimeSlotService timeSlotService;
    @Autowired
    AppointmentReplyService appointmentReplyService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    UserRepository userRepository;

    JMapper<AppointmentSchedule, AppointmentScheduleDTO> toAppointmentSchedule;
    JMapper<AppointmentScheduleDTO, AppointmentSchedule> toAppointmentScheduleDto;

    public AppointmentScheduleService() {
        this.toAppointmentSchedule = new JMapper<>(AppointmentSchedule.class, AppointmentScheduleDTO.class);
        this.toAppointmentScheduleDto = new JMapper<>(AppointmentScheduleDTO.class, AppointmentSchedule.class);
    }

    public SearchResponseDTO findByCustomerId(Integer pageIndex, Integer pageSize, Short status){
        User user = contextService.getCurrentUser();
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<AppointmentSchedule> appointmentSchedulePage;
        if(status != null){
            if(status == 1L){
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 1, (short) 2, (short) 3, (short) 5), pageRequest);
            }else if(status == 4L){
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 4), pageRequest);
            }else if(status == 6L){
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 6), pageRequest);
            }else if(status == 7L){
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 7, (short) 8), pageRequest);
            }else if(status == 9L){
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 9, (short) 10), pageRequest);
            }else{
                appointmentSchedulePage = appointmentScheduleRepository.findByCustomerId(user.getId(), pageRequest);
            }
        }else{
            appointmentSchedulePage = appointmentScheduleRepository.findByCustomerId(user.getId(), pageRequest);
        }
        if(appointmentSchedulePage.getSize() > 0){
            List<AppointmentScheduleDTO> appointmentScheduleDTOList = new ArrayList<>();
            for (AppointmentSchedule appointmentSchedule : appointmentSchedulePage){
                AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
                if(appointmentSchedule.getCustomerId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                    appointmentScheduleDTO.setCustomer(userDTO);
                }
                if(appointmentSchedule.getDoctorId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getDoctorId());
                    appointmentScheduleDTO.setDoctor(userDTO);
                }
                if(appointmentSchedule.getTimeSlotId() != null){
                    TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                    appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                    if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                        LocalDateTime now = LocalDateTime.now();
                        LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                        LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                        if(now.isAfter(timeStart) && now.isBefore(timeEnd)){
                            appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                        }else if(now.isBefore(timeStart)){
                            appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                        }else {
                            appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                        }
                    }else if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RATE.value)){
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }

                appointmentScheduleDTOList.add(appointmentScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(appointmentSchedulePage.getTotalPages(), appointmentSchedulePage.getNumber(), appointmentSchedulePage.getTotalElements(), appointmentScheduleDTOList);
        }
        return null;
    }

    public SearchResponseDTO findForAdmin(Integer pageIndex, Integer pageSize, Short status){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<AppointmentSchedule> appointmentSchedulePage;
        if(status != null){
            if(status == 4L){
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.WAITING.value), pageRequest);
            }else if(status == 5L){
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.RESET.value), pageRequest);
            }else if(status == 6L){
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.ACCEPT.value), pageRequest);
            }else if(status == 8L){
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.REJECT.value), pageRequest);
            }else if(status == 9L){
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), pageRequest);
            }else{
                appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.WAITING.value, AppointmentStatus.ACCEPT.value, AppointmentStatus.RESET.value, AppointmentStatus.REJECT.value, AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), pageRequest);
            }
        }else{
            appointmentSchedulePage = appointmentScheduleRepository.findByStatusIn(Arrays.asList(AppointmentStatus.WAITING.value, AppointmentStatus.ACCEPT.value, AppointmentStatus.RESET.value, AppointmentStatus.REJECT.value, AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), pageRequest);
        }

        if(appointmentSchedulePage.getSize() > 0){
            List<AppointmentScheduleDTO> appointmentScheduleDTOList = new ArrayList<>();
            for (AppointmentSchedule appointmentSchedule : appointmentSchedulePage){
                AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
                if(appointmentSchedule.getCustomerId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                    appointmentScheduleDTO.setCustomer(userDTO);
                }
                if(appointmentSchedule.getDoctorId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getDoctorId());
                    appointmentScheduleDTO.setDoctor(userDTO);
                }
                if(appointmentSchedule.getTimeSlotId() != null){
                    TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                    appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                    if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                        LocalDateTime now = LocalDateTime.now();
                        LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                        LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                        if(now.isAfter(timeStart) && now.isBefore(timeEnd)){
                            appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                        }else if(now.isBefore(timeStart)){
                            appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                        }else {
                            appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                        }
                    }else if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RATE.value)){
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }

                appointmentScheduleDTOList.add(appointmentScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(appointmentSchedulePage.getTotalPages(), appointmentSchedulePage.getNumber(), appointmentSchedulePage.getTotalElements(), appointmentScheduleDTOList);
        }
        return null;
    }

    public SearchResponseDTO findForDoctor(LocalDate date, Integer pageIndex, Integer pageSize){
        User user = contextService.getCurrentUser();
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<AppointmentSchedule> appointmentSchedulePage;
        if(date != null){
            appointmentSchedulePage = appointmentScheduleRepository.findByDoctorIdAndStatusInAndDate(user.getId() ,Arrays.asList(AppointmentStatus.ACCEPT.value, AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), date, pageRequest);
        }else{
            appointmentSchedulePage = appointmentScheduleRepository.findByDoctorIdAndStatusIn(user.getId() ,Arrays.asList(AppointmentStatus.ACCEPT.value, AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value), pageRequest);
        }


        if(appointmentSchedulePage.getSize() > 0){
            List<AppointmentScheduleDTO> appointmentScheduleDTOList = new ArrayList<>();
            for (AppointmentSchedule appointmentSchedule : appointmentSchedulePage){
                AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
                if(appointmentSchedule.getCustomerId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                    appointmentScheduleDTO.setCustomer(userDTO);
                }
                if(appointmentSchedule.getTimeSlotId() != null){
                    TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                    appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                    if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                        LocalDateTime now = LocalDateTime.now();
                        LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                        LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                        if(now.isAfter(timeStart) && now.isBefore(timeEnd)){
                            appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                        }else if(now.isBefore(timeStart)){
                            appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                        }else {
                            appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                        }
                    }else if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value)){
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }

                appointmentScheduleDTOList.add(appointmentScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(appointmentSchedulePage.getTotalPages(), appointmentSchedulePage.getNumber(), appointmentSchedulePage.getTotalElements(), appointmentScheduleDTOList);
        }
        return null;
    }

    // Danh sách lịch hẹn bác sĩ liên qua trong danh sách lịch hẹn
    public List<AppointmentScheduleDTO> findByDoctorIdAndDate(Long doctorId, LocalDate date, Long timeSlotId, Long appointmentId){
        List<AppointmentSchedule> appointmentScheduleList = appointmentScheduleRepository.findByDoctorIdAndStatusInAndDateAndTimeSlotIdInAndIdNot(
                doctorId,
                Arrays.asList((short) 6, (short)9, (short)10),
                date,
                Arrays.asList(timeSlotId - 4L, timeSlotId - 3L, timeSlotId - 2L, timeSlotId - 1L, timeSlotId, timeSlotId + 1L, timeSlotId + 2L, timeSlotId + 3L, timeSlotId + 4L),
                appointmentId
        );
        if(appointmentScheduleList.size() > 0){
            List<AppointmentScheduleDTO> appointmentScheduleDTOList = new ArrayList<>();
            for (AppointmentSchedule appointmentSchedule : appointmentScheduleList){
                AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
                if(appointmentSchedule.getCustomerId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                    appointmentScheduleDTO.setCustomer(userDTO);
                }
                if(appointmentSchedule.getDoctorId() != null){
                    UserDTO userDTO = userService.findById(appointmentSchedule.getDoctorId());
                    appointmentScheduleDTO.setDoctor(userDTO);
                }
                if(appointmentSchedule.getTimeSlotId() != null){
                    TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                    appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                    if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                        LocalDateTime now = LocalDateTime.now();
                        LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                        LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                        if(now.isAfter(timeStart) && now.isBefore(timeEnd)){
                            appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                        }else if(now.isBefore(timeStart)){
                            appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                        }else {
                            appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                        }
                    }else if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value)){
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }

                appointmentScheduleDTOList.add(appointmentScheduleDTO);
            }
            return appointmentScheduleDTOList;
        }
        return null;
    }

    // Kiểm tra khung giờ có bị xung đột hay không
    public Boolean checkConflictAppointment(Long timeSlotId, LocalDate date, Long doctorId, Long appointmentId){
        Long count = appointmentScheduleRepository.countByDoctorIdAndTimeSlotIdAndDateAndStatusInAndIdNot(doctorId, timeSlotId, date, Arrays.asList((short) 6, (short)9, (short)10), appointmentId);
        if(count > 0L){
            return true;
        }
        return false;
    }

    public AppointmentCountDTO getAppointmentCount(){
        AppointmentCountDTO appointmentCountDTO = new AppointmentCountDTO();
        appointmentCountDTO.setAll(countByStatus((short) 0));
        appointmentCountDTO.setComplete(countByStatus((short) 9));
        appointmentCountDTO.setActive(countByStatus((short) 6));
        appointmentCountDTO.setEditable(countByStatus((short) 1));
        appointmentCountDTO.setWaiting(countByStatus((short) 4));
        appointmentCountDTO.setCancel(countByStatus((short) 7));
        return appointmentCountDTO;
    }

    public AppointmentCountDTO getAppointmentCountForAdmin(){
        AppointmentCountDTO appointmentCountDTO = new AppointmentCountDTO();
        appointmentCountDTO.setAll(countByStatusForAdmin((short) 0));
        appointmentCountDTO.setComplete(countByStatusForAdmin((short) 9));
        appointmentCountDTO.setActive(countByStatusForAdmin((short) 6));
        appointmentCountDTO.setEditable(countByStatusForAdmin((short) 5));
        appointmentCountDTO.setWaiting(countByStatusForAdmin((short) 4));
        appointmentCountDTO.setCancel(countByStatusForAdmin((short) 8));
        return appointmentCountDTO;
    }

    public Long countByStatus(Short status){
        User user = contextService.getCurrentUser();
        // 0 : tất cả
        // 1,2,3, 5 : nháp, trả về
        // 4 : chờ duyệt
        // 6 : hoạt động
        // 7,8 : hủy bỏ
        // 9 : hoàn thành
        switch (status){
            case (short) 0:
                return appointmentScheduleRepository.countAllByCustomerId(user.getId());
            case (short) 1:
            case (short) 2:
            case (short) 3:
            case (short) 5:
                return appointmentScheduleRepository.countByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 1, (short) 2, (short) 3, (short) 5));
            case (short) 4:
                return appointmentScheduleRepository.countByCustomerIdAndStatus(user.getId(), (short) 4);
            case (short) 6:
                return appointmentScheduleRepository.countByCustomerIdAndStatus(user.getId(), (short) 6);
            case (short) 7:
            case (short) 8:
                return appointmentScheduleRepository.countByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 7, (short) 8));
            case (short) 9:
                return appointmentScheduleRepository.countByCustomerIdAndStatusIn(user.getId(), Arrays.asList((short) 9, (short) 10));
            default:
                throw new RuntimeException("Trạng thái lỗi");
        }
    }

    public Long countByStatusForAdmin(Short status){
        // 0 : tất cả
        // 4 : chờ duyệt
        // 5 : trả về
        // 6 : hoạt động
        // 8 : hủy bỏ
        // 9 : hoàn thành
        switch (status){
            case (short) 0:
                return appointmentScheduleRepository.countByStatusIn(Arrays.asList((short) 4, (short) 5, (short) 6, (short) 8, (short) 9, (short) 10));
            case (short) 4:
                return appointmentScheduleRepository.countByStatus((short) 4);
            case (short) 5:
                return appointmentScheduleRepository.countByStatus((short) 5);
            case (short) 6:
                return appointmentScheduleRepository.countByStatus((short) 6);
            case (short) 8:
                return appointmentScheduleRepository.countByStatus((short) 8);
            case (short) 9:
                return appointmentScheduleRepository.countByStatusIn(Arrays.asList((short) 9, (short) 10));
            default:
                throw new RuntimeException("Trạng thái lỗi");
        }
    }

    public AppointmentScheduleDTO findById(Long id){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(id, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
            if(appointmentSchedule.getCustomerId() != null){
                UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                appointmentScheduleDTO.setCustomer(userDTO);
            }
            if(appointmentSchedule.getDoctorId() != null){
                UserDTO userDTO = userService.findById(appointmentSchedule.getDoctorId());
                appointmentScheduleDTO.setDoctor(userDTO);
            }
            if(appointmentSchedule.getTimeSlotId() != null){
                TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                    LocalDateTime now = LocalDateTime.now();
                    LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                    LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                    if(now.isAfter(timeStart) && now.isAfter(timeEnd)){
                        appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                    }else if(now.isBefore(timeStart)){
                        appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                    }else {
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }else if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value)){
                    appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                }
            }
            return appointmentScheduleDTO;
        }
        throw new RuntimeException("Không tìm thấy lịch hẹn");
    }

    public AppointmentScheduleDTO findByIdForAdmin(Long id){
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findById(id);
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            AppointmentScheduleDTO appointmentScheduleDTO = toAppointmentScheduleDto.getDestination(appointmentSchedule);
            if(appointmentSchedule.getCustomerId() != null){
                UserDTO userDTO = userService.findById(appointmentSchedule.getCustomerId());
                appointmentScheduleDTO.setCustomer(userDTO);
            }
            if(appointmentSchedule.getDoctorId() != null){
                UserDTO userDTO = userService.findById(appointmentSchedule.getDoctorId());
                appointmentScheduleDTO.setDoctor(userDTO);
            }
            if(appointmentSchedule.getTimeSlotId() != null){
                TimeSlotDTO timeSlotDTO = timeSlotService.findById(appointmentSchedule.getTimeSlotId());
                appointmentScheduleDTO.setTimeSlot(timeSlotDTO);
                if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value)){
                    LocalDateTime now = LocalDateTime.now();
                    LocalDateTime timeStart = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeStart());
                    LocalDateTime timeEnd = LocalDateTime.of(appointmentSchedule.getDate(), timeSlotDTO.getTimeEnd());
                    if(now.isAfter(timeStart) && now.isAfter(timeEnd)){
                        appointmentScheduleDTO.setTimeEnd("Đang diễn ra");
                    }else if(now.isBefore(timeStart)){
                        appointmentScheduleDTO.setTimeEnd("Chưa diễn ra");
                    }else {
                        appointmentScheduleDTO.setTimeEnd("Đã kết thúc");
                    }
                }
            }
            return appointmentScheduleDTO;
        }
        throw new RuntimeException("Không tìm thấy lịch hẹn");
    }

    // Khách hàng chọn bác sĩ và ngày
    public AppointmentScheduleDTO createAppointmentForm1(Long appointmentId, Long doctorId, LocalDate date){
        User user = contextService.getCurrentUser();
        AppointmentSchedule appointmentSchedule;
        if(appointmentId != null){
            Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
            if(appointmentScheduleOptional.isPresent()) {
                appointmentSchedule = appointmentScheduleOptional.get();
                if(appointmentSchedule.getStatus() <= AppointmentStatus.DRAFT3.value || appointmentSchedule.getStatus().equals(AppointmentStatus.RESET.value)){
                    appointmentSchedule = appointmentScheduleOptional.get();
                    appointmentSchedule.setDoctorId(doctorId);
                    appointmentSchedule.setDate(date);
                }
            }else{
                throw new RuntimeException("Không tìm thấy lịch hẹn");
            }
        }else{
            appointmentSchedule = new AppointmentSchedule();
            appointmentSchedule.setCustomerId(user.getId());
            appointmentSchedule.setDoctorId(doctorId);
            appointmentSchedule.setDate(date);
            appointmentSchedule.setStatus(AppointmentStatus.DRAFT1.value);

        }
        return toAppointmentScheduleDto.getDestination(appointmentScheduleRepository.save(appointmentSchedule));
    }

    // Khách hàng chọn khung giờ
    public AppointmentScheduleDTO createAppointmentForm2(Long appointmentId, Long timeSlotId){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if((appointmentSchedule.getStatus() >= AppointmentStatus.DRAFT1.value && appointmentSchedule.getStatus() <= AppointmentStatus.DRAFT3.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RESET.value)){
                appointmentSchedule.setTimeSlotId(timeSlotId);
                if(appointmentSchedule.getStatus().equals(AppointmentStatus.DRAFT1.value)){
                    appointmentSchedule.setStatus(AppointmentStatus.DRAFT2.value);
                }
                return toAppointmentScheduleDto.getDestination(appointmentScheduleRepository.save(appointmentSchedule));
            }
        }
        return null;
    }

    // Khách hàng điền thông tin
    public AppointmentScheduleDTO createAppointmentForm3(Long appointmentId, String note){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if((appointmentSchedule.getStatus() >= AppointmentStatus.DRAFT2.value && appointmentSchedule.getStatus() <= AppointmentStatus.DRAFT3.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RESET.value)){
                appointmentSchedule.setNote(note);
                if(appointmentSchedule.getStatus().equals(AppointmentStatus.DRAFT2.value)){
                    appointmentSchedule.setStatus(AppointmentStatus.DRAFT3.value);
                }
                return toAppointmentScheduleDto.getDestination(appointmentScheduleRepository.save(appointmentSchedule));
            }
        }
        return null;
    }

    // Khách hàng xác nhận điều khoản
    public Boolean createAppointmentForm4(Long appointmentId){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if(appointmentSchedule.getStatus().equals(AppointmentStatus.DRAFT3.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RESET.value)){
                appointmentSchedule.setStatus(AppointmentStatus.WAITING.value);
                appointmentScheduleRepository.save(appointmentSchedule);
                NotificationDTO notificationDTO = new NotificationDTO();
                List<UserDTO> userDTOList = userService.findAllByRole(ROLE.ADMIN.value);
                for(UserDTO userDTO : userDTOList){
                    notificationDTO.setUserId(userDTO.getId());
                    notificationDTO.setTitle("Khách hàng " + user.getFullName() + " đã thêm một lịch hẹn mới");
                    notificationDTO.setTargetType(TargetType.APPOINTMENT_SCHEDULE.value);
                    notificationDTO.setActionType(ActionType.INSERT.value);
                    notificationDTO.setTargetId(appointmentId);
                    notificationDTO.setSeen(false);
                    notificationService.create(notificationDTO);
                }
                return true;
            }
        }
        return false;
    }

    // Khách hàng hủy bỏ lịch hẹn
    public Boolean cancelAppointment(Long appointmentId){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if(appointmentSchedule.getStatus().equals(AppointmentStatus.WAITING.value) || appointmentSchedule.getStatus().equals(AppointmentStatus.RESET.value)){
                appointmentSchedule.setStatus(AppointmentStatus.CANCEL.value);
                appointmentScheduleRepository.save(appointmentSchedule);
                return true;
            }
        }
        return false;
    }

    // Bác sĩ hoàn thành lịch hẹn
    public Boolean completeAppointment(Long appointmentId){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndDoctorId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if(appointmentSchedule.getStatus().equals(AppointmentStatus.ACCEPT.value) && appointmentSchedule.getDoctorId().equals(user.getId())){
                appointmentSchedule.setStatus(AppointmentStatus.COMPLETE.value);
                appointmentScheduleRepository.save(appointmentSchedule);
                NotificationDTO notificationDTO = new NotificationDTO();
                notificationDTO.setUserId(appointmentSchedule.getCustomerId());
                notificationDTO.setTitle("Lịch hẹn của bạn đã hoàn thành. Bạn có thể đánh giá lịch hẹn ngay lúc này.");
                notificationDTO.setTargetType(TargetType.APPOINTMENT_SCHEDULE.value);
                notificationDTO.setActionType(ActionType.COMPLETE.value);
                notificationDTO.setTargetId(appointmentId);
                notificationDTO.setSeen(false);
                return true;
            }
        }
        return false;
    }

    // Khách hàng đánh giá lịch hẹn
    public Boolean rateAppointment(Long appointmentId, Short star, String content){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findByIdAndCustomerId(appointmentId, user.getId());
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if(appointmentSchedule.getStatus().equals(AppointmentStatus.COMPLETE.value) && appointmentSchedule.getCustomerId().equals(user.getId()) && appointmentSchedule.getUserStarRate() == null){
                appointmentSchedule.setUserStarRate(star);
                appointmentSchedule.setUserContentRate(content);
                appointmentSchedule.setStatus(AppointmentStatus.RATE.value);
                appointmentScheduleRepository.save(appointmentSchedule);
                NotificationDTO notificationDTO = new NotificationDTO();
                notificationDTO.setUserId(user.getId());
                notificationDTO.setTitle("Bạn đã đánh giá lịch hẹn. Cảm ơn bạn vì phản hồi.");
                notificationDTO.setTargetType(TargetType.APPOINTMENT_SCHEDULE.value);
                notificationDTO.setActionType(ActionType.RATE.value);
                notificationDTO.setTargetId(appointmentId);
                notificationDTO.setSeen(false);
                return true;
            }
        }
        return false;
    }

    // Quản trị viên duyệt lịch hẹn
    public Boolean approveAppointment(Long appointmentId, Short action, String content){
        User user = contextService.getCurrentUser();
        Optional<AppointmentSchedule> appointmentScheduleOptional = appointmentScheduleRepository.findById(appointmentId);
        if(appointmentScheduleOptional.isPresent()){
            AppointmentSchedule appointmentSchedule = appointmentScheduleOptional.get();
            if(appointmentSchedule.getStatus().equals(AppointmentStatus.WAITING.value)){
                AppointmentStatus actionByStatus = AppointmentStatus.findByValue(action);
                AppointmentReplyDTO appointmentReplyDTO = new AppointmentReplyDTO();
                NotificationDTO notificationDTO = new NotificationDTO();
                notificationDTO.setUserId(appointmentSchedule.getCustomerId());
                notificationDTO.setTargetType(TargetType.APPOINTMENT_SCHEDULE.value);
                notificationDTO.setTargetId(appointmentId);
                notificationDTO.setSeen(false);
                appointmentReplyDTO.setAppointmentId(appointmentId);
                appointmentReplyDTO.setAdminId(user.getId());
                switch (actionByStatus){
                    case ACCEPT:
                        // Chấp thuận
                        appointmentSchedule.setStatus(AppointmentStatus.ACCEPT.value);
                        appointmentReplyDTO.setContent("Chấp nhận lịch hẹn");
                        notificationDTO.setActionType(ActionType.ACCEPT.value);
                        notificationDTO.setTitle("Lịch hẹn của bạn đã được chấp thuận");

                        NotificationDTO doctorNotification = new NotificationDTO();
                        doctorNotification.setUserId(appointmentSchedule.getDoctorId());
                        doctorNotification.setTargetType(TargetType.APPOINTMENT_SCHEDULE.value);
                        doctorNotification.setTargetId(appointmentId);
                        doctorNotification.setActionType(ActionType.NEW.value);
                        doctorNotification.setTitle("Bạn có lịch hẹn mới");
                        doctorNotification.setSeen(false);
                        notificationService.create(doctorNotification);
                        break;
                    case REJECT:
                        appointmentSchedule.setStatus(AppointmentStatus.REJECT.value);
                        appointmentReplyDTO.setContent(content);
                        notificationDTO.setActionType(ActionType.REJECT.value);
                        notificationDTO.setTitle("Lịch hẹn của bạn đã bị từ chối");
                        // Từ chối
                        break;
                    case RESET:
                        appointmentSchedule.setStatus(AppointmentStatus.RESET.value);
                        appointmentReplyDTO.setContent(content);
                        notificationDTO.setActionType(ActionType.RESET.value);
                        notificationDTO.setTitle("Lịch hẹn của bạn đã bị trả về");
                        // Trả về
                }
                appointmentReplyDTO.setAdminId(user.getId());
                appointmentReplyService.create(appointmentReplyDTO);
                appointmentScheduleRepository.save(appointmentSchedule);
                notificationService.create(notificationDTO);
                return true;
            }
        }
        return false;
    }

    // Thống kê
    public StatisticsDTO statistics(){
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        int currentYear = Year.now().getValue();
        int lastYear = currentYear - 1;
        Long currentYearCount = appointmentScheduleRepository.countByStatusInAndYear(Arrays.asList((short) 4, (short) 5, (short) 6, (short) 8, (short) 9, (short) 10), currentYear);
        Long lastYearCount = appointmentScheduleRepository.countByStatusInAndYear(Arrays.asList((short) 4, (short) 5, (short) 6, (short) 8, (short) 9, (short) 10), lastYear);
        statisticsDTO.setCount(currentYearCount);
        if(lastYearCount == 0){
            statisticsDTO.setPercentage((float) 100);
        }else{
            Float percentage = Float.valueOf((currentYearCount - lastYearCount) / lastYearCount);
            statisticsDTO.setPercentage(percentage);
        }
        if(currentYearCount == 0){
            statisticsDTO.setPercentage((float) 0);
        }
        return statisticsDTO;
    }

    public List<Long> yearNewAppointmentChart(List<Short> statusList){
        List<Long> chart = new ArrayList<>();
        int currentYear = Year.now().getValue();
        for (int i = 1; i <= 12; i++) {
            Long count = appointmentScheduleRepository.countByStatusInAndYearAndMonth(statusList,currentYear, i);
            chart.add(count);
        }
        return chart;
    }

    public List<Long> monthNewAppointmentChart(List<Short> statusList, Integer month){
        List<Long> chart = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        int currentMonth;
        if(month == null || month < 1 || month > 12){
            currentMonth = currentDate.getMonth().getValue();
        }else{
            currentMonth = month;
        }
        int currentYear = currentDate.getYear();
        int lengthOfMonth = currentDate.lengthOfMonth();
        for (int i = 1; i <= lengthOfMonth; i++) {
            Long count = appointmentScheduleRepository.countByStatusInAndYearAndMonthAndDay(statusList,currentYear, currentMonth, i);
            chart.add(count);
        }
        return chart;
    }

    public List<Integer> dayList(){
        List<Integer> chart = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        int lengthOfMonth = currentDate.lengthOfMonth();
        for (int i = 1; i <= lengthOfMonth; i++) {
            chart.add(i);
        }
        return chart;
    }

    public List<Long> statusList(){
        List<Long> chart = new ArrayList<>();
        Long waiting = appointmentScheduleRepository.countByStatus(AppointmentStatus.WAITING.value);
        chart.add(waiting);
        Long reset = appointmentScheduleRepository.countByStatus(AppointmentStatus.RESET.value);
        chart.add(reset);
        Long accept = appointmentScheduleRepository.countByStatus(AppointmentStatus.ACCEPT.value);
        chart.add(accept);
        Long reject = appointmentScheduleRepository.countByStatus(AppointmentStatus.REJECT.value);
        chart.add(reject);
        Long complete = appointmentScheduleRepository.countByStatusIn(Arrays.asList(AppointmentStatus.COMPLETE.value, AppointmentStatus.RATE.value));
        chart.add(complete);
        return chart;
    }

    public DoughnutChartDTO customerRanking(){
        DoughnutChartDTO doughnutChartDTO = new DoughnutChartDTO();
        List<ICustomerAppointmentRank> customerAppointmentRankList = appointmentScheduleRepository.customerRanking();
        List<String> titleList = customerAppointmentRankList.stream().map(u -> u.getFull_name()).collect(Collectors.toList());
        if(titleList.size() > 0){
            doughnutChartDTO.setTitleList(titleList);
        }else{
            doughnutChartDTO.setTitleList(new ArrayList<>());
        }
        List<Long> valueList = customerAppointmentRankList.stream().map(u-> u.getCount()).collect(Collectors.toList());
        if(valueList.size() > 0){
            doughnutChartDTO.setValueList(valueList);
        }else{
            doughnutChartDTO.setValueList(new ArrayList<>());
        }
        return doughnutChartDTO;
    }

    public DoughnutChartDTO doctorRanking(){
        DoughnutChartDTO doughnutChartDTO = new DoughnutChartDTO();
        List<ICustomerAppointmentRank> customerAppointmentRankList = appointmentScheduleRepository.doctorRanking();
        List<String> titleList = customerAppointmentRankList.stream().map(u -> u.getFull_name()).collect(Collectors.toList());
        if(titleList.size() > 0){
            doughnutChartDTO.setTitleList(titleList);
        }else{
            doughnutChartDTO.setTitleList(new ArrayList<>());
        }
        List<Long> valueList = customerAppointmentRankList.stream().map(u-> u.getCount()).collect(Collectors.toList());
        if(valueList.size() > 0){
            doughnutChartDTO.setValueList(valueList);
        }else{
            doughnutChartDTO.setValueList(new ArrayList<>());
        }
        return doughnutChartDTO;
    }

    public Long countBySpecialtyId(Long id){
        List<User> doctorList = userRepository.findBySpecialtyId(id);
        List<Long> doctorIdList = doctorList.stream().map(u -> u.getId()).collect(Collectors.toList());
        Long count = appointmentScheduleRepository.countByDoctorIdInAndStatusIn(doctorIdList, Arrays.asList((short) 4, (short) 5, (short) 6, (short) 8, (short) 9, (short) 10));
        return count;
    }

    public Double getPercentByStatusAndSpecialty(List<Short> statusList, Long id){
        List<User> doctorList = userRepository.findBySpecialtyId(id);
        List<Long> doctorIdList = doctorList.stream().map(u -> u.getId()).collect(Collectors.toList());
        Long countMin = appointmentScheduleRepository.countByDoctorIdInAndStatusIn(doctorIdList, statusList);
        Long countMax = appointmentScheduleRepository.countByDoctorIdInAndStatusIn(doctorIdList, Arrays.asList((short) 4, (short) 5, (short) 6, (short) 8, (short) 9, (short) 10));
        if(countMax == 0){
            return Double.valueOf(0);
        }
        Double percent = Double.valueOf((countMin / countMax) * 100);
        return percent;
    }
}
