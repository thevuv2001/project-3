package com.bkacad.project3.service;

import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.domain.Specialty;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.repository.UserRepository;
import com.bkacad.project3.util.PageUtil;
import com.bkacad.project3.util.SearchUtil;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    GenIdService genIdService;
    @Autowired
    SpecialtyService specialtyService;

    JMapper<UserDTO, User> toUserDTO;
    JMapper<User, UserDTO> toUser;

    public UserService(){
        this.toUserDTO = new JMapper<>(UserDTO.class, User.class);
        this.toUser = new JMapper<>(User.class, UserDTO.class);
    }

    public UserDTO findById(Long id){
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            return toUserDTO.getDestination(userOptional.get());
        }
        throw new RuntimeException("Không tìm thấy tài khoản");
    }

    public List<UserDTO> findAllByRole(Short role){
        List<User> userList = userRepository.findByRoleOrderByCreatedAtDesc(role);
        if(userList.size() > 0){
            if(role.equals(ROLE.DOCTOR.value)){
                List<SpecialtyDTO> specialtyDTOList = specialtyService.findAll();
                List<UserDTO> userDTOList = new ArrayList<>();
                for(User user : userList){
                    UserDTO userDTO = toUserDTO.getDestination(user);
                    if(userDTO.getSpecialtyId() != null && specialtyDTOList.size() > 0){
                        Optional<SpecialtyDTO> specialtyDTOOptional = specialtyDTOList.stream().filter(u -> u.getId().equals(userDTO.getSpecialtyId())).findFirst();
                        if(specialtyDTOOptional.isPresent()){
                            userDTO.setSpecialtyName(specialtyDTOOptional.get().getName());
                        }
                    }
                    userDTOList.add(userDTO);
                }
                return userDTOList;
            }else{
                return userList.stream().map(u -> toUserDTO.getDestination(u)).collect(Collectors.toList());
            }
        }
        return new ArrayList<>();
    }

    public Boolean signup(String fullName, String username, String password){
        Optional<User> userOptional = userRepository.findByUsername(username);
        if(userOptional.isPresent()){
            return false;
        }else{
            User user = new User();
            user.setId(genIdService.nextId());
            user.setUsername(username);
            user.setFullName(fullName);
            user.setPassword(passwordEncoder.encode(password));
            user.setAvatar("/images/default-user-avatar.jpg");
            user.setEnabled(true);
            user.setRole(ROLE.CUSTOMER.value);
            user.setGender(true);
            user.setIsLock(false);
            user.setCreatedAt(LocalDateTime.now());
            user.setUpdatedAt(LocalDateTime.now());

            userRepository.save(user);
            return true;
        }
    }

    public Boolean userCreate(String fullName, String username, String password, Short role){
        Optional<User> userOptional = userRepository.findByUsername(username);
        if(userOptional.isPresent()){
            return false;
        }else{
            User user = new User();
            user.setId(genIdService.nextId());
            user.setUsername(username);
            user.setFullName(fullName);
            user.setPassword(passwordEncoder.encode(password));
            user.setAvatar("/images/default-user-avatar.jpg");
            user.setEnabled(true);
            user.setRole(role);
            user.setGender(true);
            user.setIsLock(false);
            user.setCreatedAt(LocalDateTime.now());
            user.setUpdatedAt(LocalDateTime.now());

            userRepository.save(user);
            return true;
        }
    }

    public Boolean doctorCreate(String fullName, String username, String password, Short role, Long specialty){
        Optional<User> userOptional = userRepository.findByUsername(username);
        if(userOptional.isPresent()){
            return false;
        }else{
            User user = new User();
            user.setId(genIdService.nextId());
            user.setUsername(username);
            user.setFullName(fullName);
            user.setPassword(passwordEncoder.encode(password));
            user.setAvatar("/images/default-user-avatar.jpg");
            user.setEnabled(true);
            user.setRole(role);
            user.setGender(true);
            user.setIsLock(false);
            user.setSpecialtyId(specialty);
            user.setCreatedAt(LocalDateTime.now());
            user.setUpdatedAt(LocalDateTime.now());

            userRepository.save(user);
            return true;
        }
    }

    public Boolean adminLock(Long userId){
        Optional<User> userOptional = userRepository.findByIdAndIsLock(userId, false);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            user.setIsLock(true);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public Boolean adminUnlock(Long userId){
        Optional<User> userOptional = userRepository.findByIdAndIsLock(userId, true);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            user.setIsLock(false);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public List<UserDTO> findByRole(Short role){
        List<User> userList = userRepository.findByRoleOrderByCreatedAtDesc(role);
        if(userList.size() > 0){
            List<UserDTO> userDTOList = userList.stream().map(u -> toUserDTO.getDestination(u)).collect(Collectors.toList());
            return userDTOList;
        }

        return null;
    }

    public SearchResponseDTO searchByRole(Short role, String search, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        if(search == null){
            search = "";
        }
        Page<User> userPage = userRepository.searchByRole(role, search, pageRequest);
        if(userPage.getSize() > 0){
            List<UserDTO> userDTOList = userPage.stream().map(u -> toUserDTO.getDestination(u)).collect(Collectors.toList());
            return SearchUtil.prepareResponseForSearch(userPage.getTotalPages(), userPage.getNumber(), userPage.getTotalElements(), userDTOList);
        }
        return null;
    }

    public SearchResponseDTO searchDoctor(String search, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        if(search == null){
            search = "";
        }
        Page<User> userPage = userRepository.searchByRole(ROLE.DOCTOR.value, search, pageRequest);
        if(userPage.getSize() > 0){
            List<Long> specialtyIdList = userPage.stream().filter(u -> u.getSpecialtyId() != null).map(u -> u.getSpecialtyId()).collect(Collectors.toList());
            List<SpecialtyDTO> specialtyDTOList = specialtyService.findByIdIn(specialtyIdList);
            List<UserDTO> userDTOList = new ArrayList<>();
            for(User user : userPage){
                UserDTO userDTO = toUserDTO.getDestination(user);
                if(userDTO.getSpecialtyId() != null && specialtyDTOList.size() > 0){
                    Optional<SpecialtyDTO> specialtyDTOOptional = specialtyDTOList.stream().filter(u -> u.getId().equals(userDTO.getSpecialtyId())).findFirst();
                    if(specialtyDTOOptional.isPresent()){
                        userDTO.setSpecialtyName(specialtyDTOOptional.get().getName());
                    }
                }
                userDTOList.add(userDTO);
            }
            return SearchUtil.prepareResponseForSearch(userPage.getTotalPages(), userPage.getNumber(), userPage.getTotalElements(), userDTOList);
        }
        return null;
    }

    public List<UserDTO> findDoctorDontHaveSpecialty(){
        List<User> userList = userRepository.findDoctorDontHaveSpecialty();
        if(userList.size() > 0){
            List<UserDTO> userDTOList = userList.stream().map(u -> toUserDTO.getDestination(u)).collect(Collectors.toList());
            return userDTOList;
        }
        return null;
    }

    public List<UserDTO> findBySpecialtyId(Long id){
        List<User> userList = userRepository.findBySpecialtyIdOrderByCreatedAtDesc(id);
        if(userList.size() > 0){
            List<SpecialtyDTO> specialtyDTOList = specialtyService.findAll();
            List<UserDTO> userDTOList = new ArrayList<>();
            for(User user : userList){
                UserDTO userDTO = toUserDTO.getDestination(user);
                if(userDTO.getSpecialtyId() != null && specialtyDTOList.size() > 0){
                    Optional<SpecialtyDTO> specialtyDTOOptional = specialtyDTOList.stream().filter(u -> u.getId().equals(userDTO.getSpecialtyId())).findFirst();
                    if(specialtyDTOOptional.isPresent()){
                        userDTO.setSpecialtyName(specialtyDTOOptional.get().getName());
                    }
                }
                userDTOList.add(userDTO);
            }
            return userDTOList;
        }
        return new ArrayList<>();
    }

    // Thống kê
    public StatisticsDTO statistics(List<Short> roleIdList){
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        int currentYear = Year.now().getValue();
        int lastYear = currentYear - 1;
        Long currentYearCount = userRepository.countByRoleInAndYear(roleIdList, currentYear);
        Long lastYearCount = userRepository.countByRoleInAndYear(roleIdList, lastYear);
        statisticsDTO.setCount(currentYearCount);
        if(lastYearCount == 0){
            statisticsDTO.setPercentage((float) 100);
        }else{
            Float percentage = Float.valueOf((currentYearCount - lastYearCount) / lastYearCount);
            statisticsDTO.setPercentage(percentage);
        }
        if(currentYearCount == 0){
            statisticsDTO.setPercentage((float) 0);
        }
        return statisticsDTO;
    }

    public List<Long> yearNewUserChart(){
        List<Long> chart = new ArrayList<>();
        int currentYear = Year.now().getValue();
        for (int i = 1; i <= 12; i++) {
            Long count = userRepository.countByYearAndMonth(currentYear, i);
            chart.add(count);
        }
        return chart;
    }

    public Long maxStaffCount(){
        return userRepository.countByRoleInAndIsLockFalse(Arrays.asList(ROLE.DOCTOR.value, ROLE.SUPPORTER.value));
    }

    public Boolean updateInfo(Long id, String name, String avatar, String phone, Boolean gender){
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            user.setFullName(name);
            user.setAvatar(avatar);
            user.setPhone(phone);
            user.setGender(gender);
            userRepository.save(user);
            return true;
        }
        return false;
    }
}
