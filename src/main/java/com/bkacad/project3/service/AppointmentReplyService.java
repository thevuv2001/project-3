package com.bkacad.project3.service;

import com.bkacad.project3.domain.AppointmentReply;
import com.bkacad.project3.dto.AppointmentReplyDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.repository.AppointmentReplyRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppointmentReplyService {
    @Autowired
    AppointmentReplyRepository appointmentReplyRepository;
    @Autowired
    UserService userService;

    JMapper<AppointmentReply, AppointmentReplyDTO> toAppointmentReply;
    JMapper<AppointmentReplyDTO, AppointmentReply> toAppointmentReplyDto;

    public AppointmentReplyService() {
        this.toAppointmentReply = new JMapper<>(AppointmentReply.class, AppointmentReplyDTO.class);
        this.toAppointmentReplyDto = new JMapper<>(AppointmentReplyDTO.class, AppointmentReply.class);
    }

    public List<AppointmentReplyDTO> findByAppointmentId(Long id){
        List<AppointmentReply> appointmentReplyList = appointmentReplyRepository.findByAppointmentId(id);
        if(appointmentReplyList.size() > 0){
            List<AppointmentReplyDTO> appointmentReplyDTOList = new ArrayList<>();
            for(AppointmentReply appointmentReply : appointmentReplyList){
                AppointmentReplyDTO appointmentReplyDTO = toAppointmentReplyDto.getDestination(appointmentReply);
                UserDTO userDTO = userService.findById(appointmentReplyDTO.getAdminId());
                appointmentReplyDTO.setAdmin(userDTO);
                appointmentReplyDTOList.add(appointmentReplyDTO);
            }
            return appointmentReplyDTOList;
        }
        return null;
    }

    public List<AppointmentReplyDTO> findByAdminId(Long adminId){
        List<AppointmentReply> appointmentReplyList = appointmentReplyRepository.findByAdminId(adminId);
        if(appointmentReplyList.size() > 0){
            return appointmentReplyList.stream().map(u -> toAppointmentReplyDto.getDestination(u)).collect(Collectors.toList());
        }
        return null;
    }

    public AppointmentReplyDTO create(AppointmentReplyDTO appointmentReplyDTO){
        AppointmentReply appointmentReply = toAppointmentReply.getDestination(appointmentReplyDTO);
        return toAppointmentReplyDto.getDestination(appointmentReplyRepository.save(appointmentReply));
    }
}
