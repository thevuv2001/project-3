package com.bkacad.project3.service;

import com.bkacad.project3.domain.Specialty;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.repository.SpecialtyRepository;
import com.bkacad.project3.repository.UserRepository;
import com.bkacad.project3.util.SearchUtil;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SpecialtyService {
    @Autowired
    SpecialtyRepository specialtyRepository;
    @Autowired
    UserRepository userRepository;

    JMapper<Specialty, SpecialtyDTO> toSpecialty;
    JMapper<SpecialtyDTO, Specialty> toSpecialtyDto;

    public SpecialtyService() {
        this.toSpecialty = new JMapper<>(Specialty.class, SpecialtyDTO.class);
        this.toSpecialtyDto = new JMapper<>(SpecialtyDTO.class, Specialty.class);
    }

    public SearchResponseDTO search(String search, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        if(search == null){
            search = "";
        }
        Page<Specialty> specialtyPage = specialtyRepository.search(search, pageRequest);
        if(specialtyPage.getSize() > 0){
            List<SpecialtyDTO> specialtyDTOList = new ArrayList<>();
            for (Specialty specialty : specialtyPage){
                SpecialtyDTO specialtyDTO = toSpecialtyDto.getDestination(specialty);
                Long totalDoctor = userRepository.countBySpecialtyId(specialty.getId());
                specialtyDTO.setTotalDoctor(totalDoctor);
                specialtyDTOList.add(specialtyDTO);
            }
            return SearchUtil.prepareResponseForSearch(specialtyPage.getTotalPages(), specialtyPage.getNumber(), specialtyPage.getTotalElements(), specialtyDTOList);
        }
        return null;
    }

    public List<SpecialtyDTO> findAll(){
        List<Specialty> specialtyList = specialtyRepository.findAll();
        if(specialtyList.size() > 0){
            List<SpecialtyDTO> specialtyDTOList = specialtyList.stream().map(u -> toSpecialtyDto.getDestination(u)).collect(Collectors.toList());
            return specialtyDTOList;
        }
        return null;
    }

    public List<SpecialtyDTO> findByIdIn(List<Long> idList){
        List<Specialty> specialtyList = specialtyRepository.findAllById(idList);
        if(specialtyList.size() > 0){
            List<SpecialtyDTO> specialtyDTOList = specialtyList.stream().map(u -> toSpecialtyDto.getDestination(u)).collect(Collectors.toList());
            return specialtyDTOList;
        }
        return null;
    }

    public Boolean create(String name, List<Long> doctorIdList){
        Optional<Specialty> specialtyOptional = specialtyRepository.findByName(name);
        if(!specialtyOptional.isPresent()){
            Specialty specialty = new Specialty(name);
            Specialty result = specialtyRepository.save(specialty);
            if(doctorIdList != null){
                List<User> userList = userRepository.findAllById(doctorIdList);
                userList.forEach(user -> user.setSpecialtyId(result.getId()));
                userRepository.saveAll(userList);
            }
            return true;
        }
        return false;
    }

    public SpecialtyDTO findById(Long id){
        Optional<Specialty> specialtyOptional = specialtyRepository.findById(id);
        if(specialtyOptional.isPresent()){
            return toSpecialtyDto.getDestination(specialtyOptional.get());
        }
        return null;
    }

    public boolean updateName(Long id, String name){
        Optional<Specialty> specialtyOptional = specialtyRepository.findById(id);
        if(specialtyOptional.isPresent()){
            specialtyOptional.get().setName(name);
            specialtyRepository.save(specialtyOptional.get());
        }
        return true;
    }

    public boolean updateDoctor(Long id, List<Long> newDoctorIdList){
        Optional<Specialty> specialtyOptional = specialtyRepository.findById(id);
        if(specialtyOptional.isPresent()){
            List<User> userList = userRepository.findBySpecialtyIdOrderByCreatedAtDesc(id);
            List<Long> oldDoctorIdList = userList.stream().map(u -> u.getId()).collect(Collectors.toList());
            List<Long> copyNewDoctorIdList = new ArrayList<>(newDoctorIdList);
            copyNewDoctorIdList.removeAll(oldDoctorIdList);
            oldDoctorIdList.removeAll(newDoctorIdList);
            List<Long> insertDoctorIdList = copyNewDoctorIdList;
            List<Long> deleteDoctorIdList = oldDoctorIdList;
            if(insertDoctorIdList.size() > 0){
                List<User> insertDoctorList = userRepository.findAllById(insertDoctorIdList);
                if(insertDoctorList.size() > 0 && insertDoctorList.size() == insertDoctorIdList.size()){
                    insertDoctorList.forEach(u -> {
                        u.setSpecialtyId(id);
                    });
                    userRepository.saveAll(insertDoctorList);
                }
            }
            if(deleteDoctorIdList.size() > 0){
                List<User> deleteDoctorList = userRepository.findAllById(deleteDoctorIdList);
                if(deleteDoctorList.size() > 0 && deleteDoctorList.size() == deleteDoctorIdList.size()){
                    deleteDoctorList.forEach(u -> {
                        u.setSpecialtyId(null);
                    });
                    userRepository.saveAll(deleteDoctorList);
                }
            }

        }
        return true;
    }

    // Thống kê
    public StatisticsDTO statistics(){
        StatisticsDTO statisticsDTO = new StatisticsDTO();
        int currentYear = Year.now().getValue();
        int lastYear = currentYear - 1;
        Long currentYearCount = specialtyRepository.countByYear(currentYear);
        Long lastYearCount = specialtyRepository.countByYear(lastYear);
        statisticsDTO.setCount(currentYearCount);
        if(lastYearCount == 0){
            statisticsDTO.setPercentage((float) 100);
        }else{
            Float percentage = Float.valueOf((currentYearCount - lastYearCount) / lastYearCount);
            statisticsDTO.setPercentage(percentage);
        }
        if(currentYearCount == 0){
            statisticsDTO.setPercentage((float) 0);
        }
        return statisticsDTO;
    }

    public DoughnutChartDTO doughnutChart(){
        DoughnutChartDTO doughnutChartDTO = new DoughnutChartDTO();
        List<Specialty> specialtyList = specialtyRepository.findAll();
        List<String> titleList = new ArrayList<>();
        List<Long> valueList = new ArrayList<>();
        for(Specialty specialty : specialtyList){
            String title = specialty.getName();
            Long count = userRepository.countBySpecialtyId(specialty.getId());
            titleList.add(title);
            valueList.add(count);
        }
        doughnutChartDTO.setTitleList(titleList);
        doughnutChartDTO.setValueList(valueList);
        return doughnutChartDTO;
    }
}
