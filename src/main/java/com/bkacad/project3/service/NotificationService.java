package com.bkacad.project3.service;

import com.bkacad.project3.domain.Notification;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.NotificationDTO;
import com.bkacad.project3.repository.NotificationRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NotificationService {
    @Autowired
    NotificationRepository notificationRepository;
    @Autowired
    ContextService contextService;

    JMapper<Notification, NotificationDTO> toNotification;
    JMapper<NotificationDTO, Notification> toNotificationDto;

    public NotificationService() {
        this.toNotification = new JMapper<>(Notification.class, NotificationDTO.class);
        this.toNotificationDto = new JMapper<>(NotificationDTO.class, Notification.class);
    }

    public List<NotificationDTO> getNotificationByUserId(){
        User user = contextService.getCurrentUser();
        List<Notification> notificationList = notificationRepository.findByUserId(user.getId());
        return notificationList.stream().map(u -> toNotificationDto.getDestination(u)).collect(Collectors.toList());
    }

    public NotificationDTO create(NotificationDTO notificationDTO){
        Notification notification = toNotification.getDestination(notificationDTO);
        Notification result = notificationRepository.save(notification);
        return toNotificationDto.getDestination(result);
    }

    public void readNotification(Long id){
        Optional<Notification> notificationOptional = notificationRepository.findById(id);
        if(notificationOptional.isPresent()){
            Notification notification = notificationOptional.get();
            notification.setSeen(true);
            notificationRepository.save(notification);
        }
    }

    public Boolean unread(){
        User user = contextService.getCurrentUser();
        Long count = notificationRepository.countByUserIdAndSeenFalse(user.getId());
        if(count > 0){
            return true;
        }
        return false;
    }
}
