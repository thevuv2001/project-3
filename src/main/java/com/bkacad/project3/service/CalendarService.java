package com.bkacad.project3.service;

import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;

@Service
public class CalendarService {

    public String getMonth(int month) {
        List<String> monthList = Arrays.asList("Tháng 1","Tháng 2","Tháng 3","Tháng 4","Tháng 5","Tháng 6","Tháng 7","Tháng 8","Tháng 9","Tháng 10");
        return new DateFormatSymbols().getMonths()[month-1];
    }
}
