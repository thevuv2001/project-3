package com.bkacad.project3.service;

import com.bkacad.project3.domain.Role;
import com.bkacad.project3.dto.RoleDTO;
import com.bkacad.project3.repository.RoleRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    JMapper<Role, RoleDTO> toRole;
    JMapper<RoleDTO, Role> toRoleDto;

    public RoleService() {
        this.toRole = new JMapper<>(Role.class, RoleDTO.class);
        this.toRoleDto = new JMapper<>(RoleDTO.class, Role.class);
    }
}
