package com.bkacad.project3.service;

import com.bkacad.project3.constant.ScheduleType;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.domain.WorkSchedule;
import com.bkacad.project3.dto.*;
import com.bkacad.project3.repository.WorkScheduleRepository;
import com.bkacad.project3.util.SearchUtil;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WorkScheduleService {
    @Autowired
    WorkScheduleRepository workScheduleRepository;
    @Autowired
    ContextService contextService;
    @Autowired
    TimeSlotService timeSlotService;
    @Autowired
    UserService userService;

    JMapper<WorkSchedule, WorkScheduleDTO> toWorkSchedule;
    JMapper<WorkScheduleDTO, WorkSchedule> toWorkScheduleDto;

    public WorkScheduleService() {
        this.toWorkSchedule = new JMapper<>(WorkSchedule.class, WorkScheduleDTO.class);
        this.toWorkScheduleDto = new JMapper<>(WorkScheduleDTO.class, WorkSchedule.class);
    }

    public SearchResponseDTO findByStaffAndType(LocalDate date, Short type, Integer pageIndex, Integer pageSize){
        User user = contextService.getCurrentUser();
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<WorkSchedule> workSchedulePage;
        if(date != null){
            workSchedulePage = workScheduleRepository.findByStaffIdAndTypeAndDate(user.getId(), type, date, pageRequest);
        }else{
            workSchedulePage = workScheduleRepository.findByStaffIdAndType(user.getId(), type, pageRequest);
        }
        if(workSchedulePage.getSize() > 0){
            List<WorkScheduleDTO> workScheduleDTOList = new ArrayList<>();
            for(WorkSchedule workSchedule : workSchedulePage){
                WorkScheduleDTO workScheduleDTO = toWorkScheduleDto.getDestination(workSchedule);
                if(workSchedule.getTimeSlotIdStart() != null && workSchedule.getTimeSlotIdEnd() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(workSchedule.getTimeSlotIdStart(), workScheduleDTO.getTimeSlotIdEnd()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdStart())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdEnd())).findFirst();
                    workScheduleDTO.setTimeSlotDTOStart(timeSlotDTOStart.get());
                    workScheduleDTO.setTimeSlotDTOEnd(timeSlotDTOEnd.get());
                    workScheduleDTO.setTotalTime(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES));
                    workScheduleDTOList.add(workScheduleDTO);
                }
            }
            return SearchUtil.prepareResponseForSearch(workSchedulePage.getTotalPages(), workSchedulePage.getNumber(), workSchedulePage.getTotalElements(), workScheduleDTOList);
        }
        return null;
    }

    public SearchResponseDTO findOvertimeScheduleByStaffId(LocalDate date, Integer pageIndex, Integer pageSize){
        User user = contextService.getCurrentUser();
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        Page<IOvertimeSchedule> overtimeSchedulePage;
        if(date != null){
            overtimeSchedulePage = workScheduleRepository.searchDoctorOvertimeScheduleByDate(date, user.getId(), pageRequest);
        }else{
            overtimeSchedulePage = workScheduleRepository.searchDoctorOvertimeSchedule(user.getId(), pageRequest);
        }
        if(overtimeSchedulePage.getSize() > 0){
            List<OvertimeScheduleDTO> overtimeScheduleDTOList = new ArrayList<>();
            for(IOvertimeSchedule overtimeSchedule : overtimeSchedulePage){
                OvertimeScheduleDTO overtimeScheduleDTO = new OvertimeScheduleDTO();
                overtimeScheduleDTO.setStaffId(overtimeSchedule.getStaff_id());
                overtimeScheduleDTO.setDate(overtimeSchedule.getDate());
                overtimeScheduleDTO.setTimeSlotIdStart1(overtimeSchedule.getTime_slot_id_start_1());
                overtimeScheduleDTO.setTimeSlotIdEnd1(overtimeSchedule.getTime_slot_id_end_1());
                overtimeScheduleDTO.setTimeSlotIdStart2(overtimeSchedule.getTime_slot_id_start_2());
                overtimeScheduleDTO.setTimeSlotIdEnd2(overtimeSchedule.getTime_slot_id_end_2());
                if(overtimeScheduleDTO.getTimeSlotIdStart1() != null && overtimeScheduleDTO.getTimeSlotIdEnd1() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart1(), overtimeScheduleDTO.getTimeSlotIdEnd1()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart1())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd1())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart1(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd1(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd1().equals(96L)){
                        overtimeScheduleDTO.setTotalTime1(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime1(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                if(overtimeScheduleDTO.getTimeSlotIdStart2() != null && overtimeScheduleDTO.getTimeSlotIdEnd2() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart2(), overtimeScheduleDTO.getTimeSlotIdEnd2()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart2())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd2())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart2(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd2(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd2().equals(96L)){
                        overtimeScheduleDTO.setTotalTime2(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime2(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                overtimeScheduleDTOList.add(overtimeScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(overtimeSchedulePage.getTotalPages(), overtimeSchedulePage.getNumber(), overtimeSchedulePage.getTotalElements(), overtimeScheduleDTOList);
        }
        return null;
    }

    public boolean createHolidaySchedule(Long timeSlotIdStart, Long timeSlotIdEnd, LocalDate date){
        User user = contextService.getCurrentUser();
        Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findHolidayScheduleByDate(date, user.getId());
        if(!workScheduleOptional.isPresent()){
            WorkSchedule workSchedule = new WorkSchedule();
            workSchedule.setTimeSlotIdStart(timeSlotIdStart);
            workSchedule.setTimeSlotIdEnd(timeSlotIdEnd);
            workSchedule.setType(ScheduleType.HOLIDAY.value);
            workSchedule.setDate(date);
            workSchedule.setStaffId(user.getId());
            workScheduleRepository.save(workSchedule);
            return true;
        }
        return false;
    }

    public boolean updateWorkSchedule(Long id, Long timeSlotIdStart, Long timeSlotIdEnd, Short type){
        User user = contextService.getCurrentUser();
        Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findByIdAndStaffId(id, user.getId());
        if(workScheduleOptional.isPresent()){
            WorkSchedule workSchedule = workScheduleOptional.get();
            workSchedule.setTimeSlotIdStart(timeSlotIdStart);
            workSchedule.setTimeSlotIdEnd(timeSlotIdEnd);
            workScheduleRepository.save(workSchedule);
            return true;
        }
        return false;
    }

    public boolean deleteWorkSchedule(Long id){
        User user = contextService.getCurrentUser();
        Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findByIdAndStaffId(id, user.getId());
        if(workScheduleOptional.isPresent()){
            workScheduleRepository.delete(workScheduleOptional.get());
            return true;
        }
        return false;
    }

    public List<WorkScheduleDTO> todayDoctorSchedule(Short type){
        User user = contextService.getCurrentUser();
        List<WorkSchedule> workScheduleList = workScheduleRepository.findTodayDoctorSchedule(type, user.getId());
        if(workScheduleList.size() > 0){
            List<WorkScheduleDTO> workScheduleDTOList = new ArrayList<>();
            for(WorkSchedule workSchedule :workScheduleList){
                WorkScheduleDTO workScheduleDTO = toWorkScheduleDto.getDestination(workSchedule);
                if(workSchedule.getTimeSlotIdStart() != null && workSchedule.getTimeSlotIdEnd() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(workSchedule.getTimeSlotIdStart(), workScheduleDTO.getTimeSlotIdEnd()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdStart())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdEnd())).findFirst();
                    workScheduleDTO.setTimeSlotDTOStart(timeSlotDTOStart.get());
                    workScheduleDTO.setTimeSlotDTOEnd(timeSlotDTOEnd.get());
                    workScheduleDTO.setTotalTime(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES));
                    workScheduleDTOList.add(workScheduleDTO);
                }
            }
            return workScheduleDTOList;
        }
        return null;
    }

    public List<OvertimeScheduleDTO> todayDoctorOvertimeSchedule(Short type){
        User user = contextService.getCurrentUser();
        List<IOvertimeSchedule> overtimeSchedulePage = workScheduleRepository.findTodayDoctorOvertimeSchedule(user.getId());
        if(overtimeSchedulePage.size() > 0){
            List<OvertimeScheduleDTO> overtimeScheduleDTOList = new ArrayList<>();
            for(IOvertimeSchedule overtimeSchedule : overtimeSchedulePage){
                OvertimeScheduleDTO overtimeScheduleDTO = new OvertimeScheduleDTO();
                overtimeScheduleDTO.setStaffId(overtimeSchedule.getStaff_id());
                overtimeScheduleDTO.setDate(overtimeSchedule.getDate());
                overtimeScheduleDTO.setTimeSlotIdStart1(overtimeSchedule.getTime_slot_id_start_1());
                overtimeScheduleDTO.setTimeSlotIdEnd1(overtimeSchedule.getTime_slot_id_end_1());
                overtimeScheduleDTO.setTimeSlotIdStart2(overtimeSchedule.getTime_slot_id_start_2());
                overtimeScheduleDTO.setTimeSlotIdEnd2(overtimeSchedule.getTime_slot_id_end_2());
                if(overtimeScheduleDTO.getTimeSlotIdStart1() != null && overtimeScheduleDTO.getTimeSlotIdEnd1() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart1(), overtimeScheduleDTO.getTimeSlotIdEnd1()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart1())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd1())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart1(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd1(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd1().equals(96L)){
                        overtimeScheduleDTO.setTotalTime1(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime1(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                if(overtimeScheduleDTO.getTimeSlotIdStart2() != null && overtimeScheduleDTO.getTimeSlotIdEnd2() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart2(), overtimeScheduleDTO.getTimeSlotIdEnd2()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart2())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd2())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart2(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd2(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd2().equals(96L)){
                        overtimeScheduleDTO.setTotalTime2(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime2(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                overtimeScheduleDTOList.add(overtimeScheduleDTO);
            }
            return overtimeScheduleDTOList;
        }
        return null;
    }

    public boolean createOvertimeSchedule(Boolean morningShift, Long morningTimeSlotStart, Long morningTimeSlotEnd, Boolean nightShift, Long nightTimeSlotStart, Long nightTimeSlotEnd, LocalDate date){
        User user = contextService.getCurrentUser();
        Boolean isMoringInsert = false;
        if(morningShift != null){
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findMorningOverTimeScheduleBy(date, user.getId());
            if(!workScheduleOptional.isPresent()){
                WorkSchedule workSchedule = new WorkSchedule();
                workSchedule.setTimeSlotIdStart(morningTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(morningTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
                isMoringInsert  = true;
            }else{
                return false;
            }
        }
        if(nightShift != null){
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findNightOverTimeScheduleBy(date, user.getId());
            if(!workScheduleOptional.isPresent()){
                WorkSchedule workSchedule = new WorkSchedule();
                workSchedule.setTimeSlotIdStart(nightTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(nightTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
                return true;
            }else{
                return false;
            }
        }
        if(isMoringInsert == true){
            return true;
        }
        return false;
    }

    public boolean updateOvertimeSchedule(Boolean morningShift, Long morningTimeSlotStart, Long morningTimeSlotEnd, Boolean nightShift, Long nightTimeSlotStart, Long nightTimeSlotEnd, LocalDate date){
        User user = contextService.getCurrentUser();
        Boolean isMoringInsert = false;
        // Sửa hoặc tạo mới
        if(morningShift != null){
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findMorningOverTimeScheduleBy(date, user.getId());
            if(workScheduleOptional.isPresent()){
                WorkSchedule workSchedule = workScheduleOptional.get();
                workSchedule.setTimeSlotIdStart(morningTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(morningTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
            }else{
                WorkSchedule workSchedule = new WorkSchedule();
                workSchedule.setTimeSlotIdStart(morningTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(morningTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
            }
        }else{
            // Xóa
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findMorningOverTimeScheduleBy(date, user.getId());
            if(workScheduleOptional.isPresent()){
                workScheduleRepository.delete(workScheduleOptional.get());
            }
        }
        if(nightShift != null){
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findNightOverTimeScheduleBy(date, user.getId());
            if(workScheduleOptional.isPresent()){
                WorkSchedule workSchedule = workScheduleOptional.get();
                workSchedule.setTimeSlotIdStart(nightTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(nightTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
                return true;
            }else{
                WorkSchedule workSchedule = new WorkSchedule();
                workSchedule.setTimeSlotIdStart(nightTimeSlotStart);
                workSchedule.setTimeSlotIdEnd(nightTimeSlotEnd);
                workSchedule.setType(ScheduleType.OVERTIME.value);
                workSchedule.setDate(date);
                workSchedule.setStaffId(user.getId());
                workScheduleRepository.save(workSchedule);
            }
        }else{
            // Xóa
            Optional<WorkSchedule> workScheduleOptional = workScheduleRepository.findNightOverTimeScheduleBy(date, user.getId());
            if(workScheduleOptional.isPresent()){
                workScheduleRepository.delete(workScheduleOptional.get());
            }
        }
        return true;
    }

    public boolean deleteOvertimeSchedule(LocalDate date){
        if(date == null){
            return false;
        }
        User user = contextService.getCurrentUser();
        Optional<WorkSchedule> morningWorkScheduleOptional = workScheduleRepository.findMorningOverTimeScheduleBy(date, user.getId());
        if(morningWorkScheduleOptional.isPresent()){
            workScheduleRepository.delete(morningWorkScheduleOptional.get());
        }
        Optional<WorkSchedule> nightWorkScheduleOptional = workScheduleRepository.findNightOverTimeScheduleBy(date, user.getId());
        if(nightWorkScheduleOptional.isPresent()){
            workScheduleRepository.delete(nightWorkScheduleOptional.get());
        }
        return true;
    }

    public SearchResponseDTO findHolidayScheduleForAdmin(LocalDate date, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("id").descending());
        Page<WorkSchedule> workSchedulePage;
        if(date != null){
            workSchedulePage = workScheduleRepository.findByTypeAndDate(ScheduleType.HOLIDAY.value, date, pageRequest);
        }else{
            workSchedulePage = workScheduleRepository.findByType(ScheduleType.HOLIDAY.value, pageRequest);
        }
        if(workSchedulePage.getSize() > 0){
            List<WorkScheduleDTO> workScheduleDTOList = new ArrayList<>();
            for(WorkSchedule workSchedule : workSchedulePage){
                WorkScheduleDTO workScheduleDTO = toWorkScheduleDto.getDestination(workSchedule);
                if(workSchedule.getTimeSlotIdStart() != null && workSchedule.getTimeSlotIdEnd() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(workSchedule.getTimeSlotIdStart(), workScheduleDTO.getTimeSlotIdEnd()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdStart())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(workSchedule.getTimeSlotIdEnd())).findFirst();
                    workScheduleDTO.setTimeSlotDTOStart(timeSlotDTOStart.get());
                    workScheduleDTO.setTimeSlotDTOEnd(timeSlotDTOEnd.get());
                    workScheduleDTO.setTotalTime(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES));

                }
                if(workSchedule.getStaffId() != null){
                    UserDTO userDTO = userService.findById(workSchedule.getStaffId());
                    workScheduleDTO.setStaff(userDTO);
                }
                workScheduleDTOList.add(workScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(workSchedulePage.getTotalPages(), workSchedulePage.getNumber(), workSchedulePage.getTotalElements(), workScheduleDTOList);
        }
        return null;
    }

    public SearchResponseDTO findOvertimeScheduleForAdmin(LocalDate date, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize);
        Page<IOvertimeSchedule> overtimeSchedulePage;
        if(date != null){
            overtimeSchedulePage = workScheduleRepository.searchOvertimeScheduleByDateForAdmin(date,pageRequest);
        }else{
            overtimeSchedulePage = workScheduleRepository.searchOvertimeScheduleForAdmin(pageRequest);
        }
        if(overtimeSchedulePage.getSize() > 0){
            List<OvertimeScheduleDTO> overtimeScheduleDTOList = new ArrayList<>();
            for(IOvertimeSchedule overtimeSchedule : overtimeSchedulePage){
                OvertimeScheduleDTO overtimeScheduleDTO = new OvertimeScheduleDTO();
                overtimeScheduleDTO.setStaffId(overtimeSchedule.getStaff_id());
                overtimeScheduleDTO.setDate(overtimeSchedule.getDate());
                overtimeScheduleDTO.setTimeSlotIdStart1(overtimeSchedule.getTime_slot_id_start_1());
                overtimeScheduleDTO.setTimeSlotIdEnd1(overtimeSchedule.getTime_slot_id_end_1());
                overtimeScheduleDTO.setTimeSlotIdStart2(overtimeSchedule.getTime_slot_id_start_2());
                overtimeScheduleDTO.setTimeSlotIdEnd2(overtimeSchedule.getTime_slot_id_end_2());
                if(overtimeScheduleDTO.getTimeSlotIdStart1() != null && overtimeScheduleDTO.getTimeSlotIdEnd1() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart1(), overtimeScheduleDTO.getTimeSlotIdEnd1()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart1())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd1())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart1(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd1(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd1().equals(96L)){
                        overtimeScheduleDTO.setTotalTime1(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime1(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                if(overtimeScheduleDTO.getTimeSlotIdStart2() != null && overtimeScheduleDTO.getTimeSlotIdEnd2() != null){
                    List<TimeSlotDTO> timeSlotDTOList = timeSlotService.findByIdIn(Arrays.asList(overtimeScheduleDTO.getTimeSlotIdStart2(), overtimeScheduleDTO.getTimeSlotIdEnd2()));
                    Optional<TimeSlotDTO> timeSlotDTOStart = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdStart2())).findFirst();
                    Optional<TimeSlotDTO> timeSlotDTOEnd = timeSlotDTOList.stream().filter(u -> u.getId().equals(overtimeScheduleDTO.getTimeSlotIdEnd2())).findFirst();
                    overtimeScheduleDTO.setTimeSlotDTOStart2(timeSlotDTOStart.get());
                    overtimeScheduleDTO.setTimeSlotDTOEnd2(timeSlotDTOEnd.get());
                    if(overtimeScheduleDTO.getTimeSlotIdEnd2().equals(96L)){
                        overtimeScheduleDTO.setTotalTime2(1440L - Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }else{
                        overtimeScheduleDTO.setTotalTime2(Math.abs(timeSlotDTOStart.get().getTimeStart().until(timeSlotDTOEnd.get().getTimeEnd(), ChronoUnit.MINUTES)));
                    }
                }
                if(overtimeScheduleDTO.getStaffId() != null){
                    UserDTO userDTO = userService.findById(overtimeScheduleDTO.getStaffId());
                    overtimeScheduleDTO.setStaff(userDTO);
                }
                overtimeScheduleDTOList.add(overtimeScheduleDTO);
            }
            return SearchUtil.prepareResponseForSearch(overtimeSchedulePage.getTotalPages(), overtimeSchedulePage.getNumber(), overtimeSchedulePage.getTotalElements(), overtimeScheduleDTOList);
        }
        return null;
    }
}

