package com.bkacad.project3.service;

import com.bkacad.project3.constant.ScheduleType;
import com.bkacad.project3.domain.AppointmentSchedule;
import com.bkacad.project3.domain.TimeSlot;
import com.bkacad.project3.domain.WorkSchedule;
import com.bkacad.project3.dto.TimeSlotDTO;
import com.bkacad.project3.repository.AppointmentScheduleRepository;
import com.bkacad.project3.repository.TimeSlotRepository;
import com.bkacad.project3.repository.WorkScheduleRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TimeSlotService {
    @Autowired
    TimeSlotRepository timeSlotRepository;
    @Autowired
    WorkScheduleRepository workScheduleRepository;
    @Autowired
    AppointmentScheduleRepository appointmentScheduleRepository;

    JMapper<TimeSlot, TimeSlotDTO> toTimeSlot;
    JMapper<TimeSlotDTO, TimeSlot> toTimeSlotDto;

    public TimeSlotService() {
        this.toTimeSlot = new JMapper<>(TimeSlot.class, TimeSlotDTO.class);
        this.toTimeSlotDto = new JMapper<>(TimeSlotDTO.class, TimeSlot.class);
    }

    public List<TimeSlotDTO> findAll(){
        List<TimeSlot> timeSlotList = timeSlotRepository.findAll();
        if(timeSlotList.size() > 0){
            List<TimeSlotDTO> timeSlotDTOList = timeSlotList.stream().map(u -> toTimeSlotDto.getDestination(u)).collect(Collectors.toList());
            timeSlotDTOList.forEach(u -> {
                u.setCanRegister(true);
            });
            return timeSlotDTOList;
        }
        return null;
    }

    public TimeSlotDTO findById(Long id){
        Optional<TimeSlot> timeSlotOptional = timeSlotRepository.findById(id);
        if(timeSlotOptional.isPresent()){
            return toTimeSlotDto.getDestination(timeSlotOptional.get());
        }
        return null;
    }

    public List<TimeSlotDTO> findByIdIn(List<Long> idList){
        List<TimeSlot> timeSlotList = timeSlotRepository.findAllById(idList);
        if(timeSlotList.size() > 0){
            List<TimeSlotDTO> timeSlotDTOList = timeSlotList.stream().map(u -> toTimeSlotDto.getDestination(u)).collect(Collectors.toList());
            return timeSlotDTOList;
        }
        return null;
    }

    public List<TimeSlotDTO> findByIsRequired(Boolean isRequired){
        List<TimeSlot> timeSlotList = timeSlotRepository.findByIsRequired(isRequired);
        if(timeSlotList.size() > 0){
            List<TimeSlotDTO> timeSlotDTOList = timeSlotList.stream().map(u -> toTimeSlotDto.getDestination(u)).collect(Collectors.toList());
            return timeSlotDTOList;
        }
        return null;
    }

    public List<TimeSlotDTO> findByShift(Boolean isMorning){
        List<TimeSlot> timeSlotList;
        if(isMorning){
            timeSlotList = timeSlotRepository.findMorningShift();
        }else{
            timeSlotList = timeSlotRepository.findNightShift();
        }
        return timeSlotList.stream().map(u -> toTimeSlotDto.getDestination(u)).collect(Collectors.toList());
    }

    public List<TimeSlotDTO> findDoctorWorkTimeSlot(Long doctorId, LocalDate date){
        // Tìm giờ hành chính
        List<TimeSlot> timeSlotList = timeSlotRepository.findAll();
        List<TimeSlotDTO> timeSlotDTOList = timeSlotList.stream().map(u -> toTimeSlotDto.getDestination(u)).collect(Collectors.toList());
        timeSlotDTOList.forEach(u -> {
            if(u.getIsRequired()){
                u.setCanRegister(true);
            }else{
                u.setCanRegister(false);
            }
        });
        // Tìm lịch nghỉ
        List<WorkSchedule> holidaySchedule = workScheduleRepository.findByStaffIdAndTypeAndDate(doctorId, ScheduleType.HOLIDAY.value, date);
        // Tìm lịch tăng ca
        List<WorkSchedule> overTimeSchedule = workScheduleRepository.findByStaffIdAndTypeAndDate(doctorId, ScheduleType.OVERTIME.value, date);
        // Tìm lịch hẹn
        List<AppointmentSchedule> appointmentScheduleList = appointmentScheduleRepository.findByDoctorIdAndStatusInAndDate(doctorId, Arrays.asList((short) 6, (short) 9), date);

        if(holidaySchedule.size() > 0){
            for(WorkSchedule workSchedule : holidaySchedule){
                Long timeSlotIdStart = workSchedule.getTimeSlotIdStart();
                Long timeSlotIdEnd = workSchedule.getTimeSlotIdEnd();
                for(TimeSlotDTO timeSlotDTO : timeSlotDTOList){
                    Long timeSlotId = timeSlotDTO.getId();
                    if(timeSlotId >= timeSlotIdStart && timeSlotId <= timeSlotIdEnd){
                        timeSlotDTO.setCanRegister(false);
                    }
                }
            }
        }
        if(overTimeSchedule.size() > 0){
            for(WorkSchedule workSchedule : overTimeSchedule){
                Long timeSlotIdStart = workSchedule.getTimeSlotIdStart();
                Long timeSlotIdEnd = workSchedule.getTimeSlotIdEnd();
                for(TimeSlotDTO timeSlotDTO : timeSlotDTOList){
                    Long timeSlotId = timeSlotDTO.getId();
                    if(timeSlotId >= timeSlotIdStart && timeSlotId <= timeSlotIdEnd){
                        timeSlotDTO.setCanRegister(true);
                    }
                }
            }
        }
        if(appointmentScheduleList.size() > 0){
            for (AppointmentSchedule appointmentSchedule : appointmentScheduleList){
                for(TimeSlotDTO timeSlotDTO : timeSlotDTOList){
                    Long timeSlotId = timeSlotDTO.getId();
                    if(timeSlotId.equals(appointmentSchedule.getTimeSlotId())){
                        timeSlotDTO.setCanRegister(false);
                    }
                }
            }
        }
        return timeSlotDTOList;
    }
}

