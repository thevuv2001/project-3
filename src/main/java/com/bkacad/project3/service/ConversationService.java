package com.bkacad.project3.service;

import com.bkacad.project3.constant.ROLE;
import com.bkacad.project3.domain.Conversation;
import com.bkacad.project3.domain.Message;
import com.bkacad.project3.domain.User;
import com.bkacad.project3.dto.ConversationDTO;
import com.bkacad.project3.dto.SearchResponseDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.repository.ConversationRepository;
import com.bkacad.project3.util.SearchUtil;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConversationService {
    @Autowired
    ConversationRepository conversationRepository;
    @Autowired
    UserService userService;
    @Autowired
    ContextService contextService;

    JMapper<Conversation, ConversationDTO> toConversation;
    JMapper<ConversationDTO, Conversation> toConversationDto;

    public ConversationService() {
        this.toConversation = new JMapper<>(Conversation.class, ConversationDTO.class);
        this.toConversationDto = new JMapper<>(ConversationDTO.class, Conversation.class);
    }

    public ConversationDTO createConversation(){
        User user = contextService.getCurrentUser();
        Optional<Conversation> conversationOptional = conversationRepository.findByCustomerId(user.getId());
        if(conversationOptional.isPresent()){
            return toConversationDto.getDestination(conversationOptional.get());
        }else{
            List<UserDTO> userDTOList = userService.findAllByRole(ROLE.SUPPORTER.getValue());
            UserDTO userDTO = userDTOList.get(0);
            Conversation conversation = new Conversation();
            conversation.setCustomerId(user.getId());
            conversation.setSupporterId(userDTO.getId());
            conversation.setCustomerUnread(0L);
            conversation.setSupporterUnread(0L);
            return toConversationDto.getDestination(conversationRepository.save(conversation));
        }
    }

    public SearchResponseDTO findBySupporter(String search, Integer pageIndex, Integer pageSize){
        PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by("updatedAt").descending());
        User user = contextService.getCurrentUser();
        Page<Conversation> conversationPage = conversationRepository.findBySupporterId(user.getId(), pageRequest);
        List<ConversationDTO> conversationDTOList = new ArrayList<>();
        if(conversationPage.getSize() > 0){
            for(Conversation conversation : conversationPage){
                ConversationDTO conversationDTO = toConversationDto.getDestination(conversation);
                UserDTO userDTO = userService.findById(conversation.getCustomerId());
                conversationDTO.setCustomer(userDTO);
                conversationDTOList.add(conversationDTO);
            }
            return SearchUtil.prepareResponseForSearch(conversationPage.getTotalPages(), conversationPage.getNumber(), conversationPage.getTotalElements(), conversationDTOList);
        }
        return null;
    }

    public ConversationDTO findById(Long id){
        Optional<Conversation> conversationOptional = conversationRepository.findById(id);
        if(conversationOptional.isPresent()) {
            return toConversationDto.getDestination(conversationOptional.get());
        }
        return null;
    }

    public void updateLastMessage(Long conversationId, String content){
        Optional<Conversation> conversationOptional = conversationRepository.findById(conversationId);
        if(conversationOptional.isPresent()) {
           Conversation conversation = conversationOptional.get();
           conversation.setLastMessage(content);
           conversationRepository.save(conversation);
        }
    }
}
