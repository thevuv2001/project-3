package com.bkacad.project3.service;

import com.bkacad.project3.domain.ClientChatMessage;
import com.bkacad.project3.domain.Message;
import com.bkacad.project3.dto.MessageDTO;
import com.bkacad.project3.dto.UserDTO;
import com.bkacad.project3.repository.MessageRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageService {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserService userService;
    @Autowired
    ConversationService conversationService;

    JMapper<Message, MessageDTO> toMessage;
    JMapper<MessageDTO, Message> toMessageDto;

    public MessageService() {
        this.toMessage = new JMapper<>(Message.class, MessageDTO.class);
        this.toMessageDto = new JMapper<>(MessageDTO.class, Message.class);
    }

    public ClientChatMessage sendMessage(ClientChatMessage clientChatMessage){
        UserDTO userDTO = userService.findById(clientChatMessage.getSenderId());
        Message message = new Message();
        message.setContent(clientChatMessage.getContent());
        message.setSenderId(userDTO.getId());
        message.setConversationId(clientChatMessage.getConversationId());
        Message result = messageRepository.save(message);
        clientChatMessage.setId(result.getId());
        clientChatMessage.setCreatedAt(result.getCreatedAt());
        clientChatMessage.setSenderName(userDTO.getFullName());
        conversationService.updateLastMessage(clientChatMessage.getConversationId(), clientChatMessage.getContent());
        return clientChatMessage;
    }

    public List<MessageDTO> getMessageByConversation(Long id){
        List<Message> messageList = messageRepository.findByConversationId(id);
        return messageList.stream().map(u -> toMessageDto.getDestination(u)).collect(Collectors.toList());
    }
}
