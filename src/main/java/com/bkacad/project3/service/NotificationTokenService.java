package com.bkacad.project3.service;

import com.bkacad.project3.domain.NotificationToken;
import com.bkacad.project3.dto.NotificationTokenDTO;
import com.bkacad.project3.repository.NotificationTokenRepository;
import com.googlecode.jmapper.JMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class NotificationTokenService {
    @Autowired
    NotificationTokenRepository notificationTokenRepository;

    JMapper<NotificationToken, NotificationTokenDTO> toNotificationToken;
    JMapper<NotificationTokenDTO, NotificationToken> toNotificationTokenDto;
}
