package com.bkacad.project3.constant;

public enum ROLE {
    ADMIN((short) 1),
    DOCTOR((short) 2),
    SUPPORTER((short) 3),
    CUSTOMER((short) 4);

    public Short value;

    ROLE(Short value) {
        this.value = value;
    }

    public Short getValue() {
        return value;
    }

    public static ROLE findByValue(Short param) {
        for (ROLE v : values()) {
            if (v.getValue().equals(param)) {
                return v;
            }
        }
        return null;
    }

    public static String findNameByValue(Short param) {
        for (ROLE v : values()) {
            if (v.getValue().equals(param)) {
                return v.name();
            }
        }
        return null;
    }
}
