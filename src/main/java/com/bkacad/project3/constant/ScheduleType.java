package com.bkacad.project3.constant;

public enum ScheduleType {
    HOLIDAY((short) 1),
    OVERTIME((short) 2);

    public Short value;

    ScheduleType(Short value) {
        this.value = value;
    }

    public Short getValue() {
        return value;
    }

    public static ScheduleType findByValue(Short abbr) {
        for (ScheduleType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v;
            }
        }
        return null;
    }

    public static String findNameByValue(Short abbr) {
        for (ScheduleType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v.name();
            }
        }
        return null;
    }
}
