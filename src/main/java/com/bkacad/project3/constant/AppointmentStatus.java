package com.bkacad.project3.constant;

public enum AppointmentStatus {
    DRAFT1((short) 1), // xong chọn bác sĩ
    DRAFT2((short) 2), // xong chọn khung giờ
    DRAFT3((short) 3), // xong điền thông tin
    WAITING((short) 4), // Lịch hẹn đang chờ duyệt
    RESET((short) 5), // Lịch hẹn được trả về với mong muốn khách hàng đặt lại
    ACCEPT((short) 6), // Lịch hẹn được chấp nhận
    CANCEL((short) 7), // lịch hẹn bị người dùng hủy
    REJECT((short) 8), // Lịch hẹn bị admin từ chối
    COMPLETE((short) 9), // Lịch hẹn hoàn thành
    RATE((short) 10) // Lịch hẹn đã được đánh giá
    ;

    public Short value;

    AppointmentStatus(Short value) {
        this.value = value;
    }

    public Short getValue() {
        return value;
    }

    public static AppointmentStatus findByValue(Short abbr) {
        for (AppointmentStatus v : values()) {
            if (v.getValue().equals(abbr)) {
                return v;
            }
        }
        return null;
    }

    public static String findNameByValue(Short abbr) {
        for (AppointmentStatus v : values()) {
            if (v.getValue().equals(abbr)) {
                return v.name();
            }
        }
        return null;
    }
}
