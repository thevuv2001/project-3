package com.bkacad.project3.constant;

public class EntityName {
    public final static String USER = "tbl_user";
    public final static String ROLE = "tbl_role";
    public final static String SPECIALTY = "tbl_specialty";
    public final static String TIME_SLOT = "tbl_time_slot";
    public final static String WORK_SCHEDULE = "tbl_work_schedule";
    public final static String APPOINTMENT_SCHEDULE = "tbl_appointment_schedule";
    public final static String APPOINTMENT_REPLY = "tbl_appointment_reply";
    public final static String NOTIFICATION = "tbl_notification";
    public final static String NOTIFICATION_TOKEN = "tbl_notification_token";
    public final static String CONVERSATION = "tbl_conversation";
    public final static String MESSAGE = "tbl_message";
}
