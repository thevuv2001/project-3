package com.bkacad.project3.constant;

public enum ActionType {
    INSERT((short) 1),
    UPDATE((short) 2),
    DELETE((short) 3),
    ACCEPT((short) 4),
    WAITING((short) 5),
    REJECT((short) 6),
    RESET((short) 7),
    COMPLETE((short) 8),
    RATE((short) 9),
    NEW((short) 10);

    public Short value;

    ActionType(Short value) {
        this.value = value;
    }

    public Short getValue() {
        return value;
    }

    public static ActionType findByValue(Short abbr) {
        for (ActionType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v;
            }
        }
        return null;
    }

    public static String findNameByValue(Short abbr) {
        for (ActionType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v.name();
            }
        }
        return null;
    }
}
