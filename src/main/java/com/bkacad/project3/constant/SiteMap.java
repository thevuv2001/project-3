package com.bkacad.project3.constant;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum SiteMap {
    ADMIN("/quan-tri-vien", ROLE.ADMIN),
    DOCTOR("/bac-si", ROLE.DOCTOR),
    SUPPORTER("/cham-soc-khach-hang", ROLE.SUPPORTER),
    CUSTOMER("/khach-hang", ROLE.CUSTOMER);


    String uri;
    ROLE roleName;

    SiteMap(String uri, ROLE roleName) {
        this.uri = uri;
        this.roleName = roleName;
    }

    public static SiteMap findUri(String requestUri) {
        SiteMap result = null;
        for (SiteMap siteMap : SiteMap.values()) {
            if (siteMap.getUri().equals(requestUri)) {
                result = siteMap;
            }
        }
        if (result == null) {
            for (SiteMap siteMap : SiteMap.values()) {
                if (requestUri.startsWith(siteMap.getUri())) {
                    result = siteMap;
                }
            }
        }
        return result;
    }

    public static List<SiteMap> findSiteMapByRole(ROLE roleName) {
        List<SiteMap> siteMaps = new ArrayList<>();
        for (SiteMap siteMap : SiteMap.values()) {
            if (siteMap.getRoleName().equals(roleName)) {
                siteMaps.add(siteMap);
            }
        }
        return siteMaps;
    }
}
