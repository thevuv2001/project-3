package com.bkacad.project3.constant;

public enum TargetType {
    APPOINTMENT_SCHEDULE((short) 1),
    HOLIDAY_SCHEDULE((short) 2),
    OVERTIME_SCHEDULE((short) 3);

    public Short value;

    TargetType(Short value) {
        this.value = value;
    }

    public Short getValue() {
        return value;
    }

    public static TargetType findByValue(Short abbr) {
        for (TargetType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v;
            }
        }
        return null;
    }

    public static String findNameByValue(Short abbr) {
        for (TargetType v : values()) {
            if (v.getValue().equals(abbr)) {
                return v.name();
            }
        }
        return null;
    }
}
