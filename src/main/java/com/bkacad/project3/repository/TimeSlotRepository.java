package com.bkacad.project3.repository;

import com.bkacad.project3.domain.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long>, JpaSpecificationExecutor<TimeSlot> {
    List<TimeSlot> findByIsRequired(Boolean isRequired);

    @Query(value = "select * from #{#entityName} where time_end <= '12:00:00' and is_required = false", nativeQuery = true)
    List<TimeSlot> findMorningShift();

    @Query(value = "select * from #{#entityName} where time_end > '12:00:00' and is_required = false", nativeQuery = true)
    List<TimeSlot> findNightShift();

}
