package com.bkacad.project3.repository;

import com.bkacad.project3.domain.WorkSchedule;
import com.bkacad.project3.dto.IOvertimeSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface WorkScheduleRepository extends JpaRepository<WorkSchedule, Long>, JpaSpecificationExecutor<WorkSchedule> {
    Page<WorkSchedule> findByStaffIdAndType(Long staffId, Short type, Pageable pageable);

    Page<WorkSchedule> findByStaffIdAndTypeAndDate(Long staffId, Short type, LocalDate date, Pageable pageable);

    Page<WorkSchedule> findByType(Short type, Pageable pageable);

    Page<WorkSchedule> findByTypeAndDate(Short type, LocalDate date, Pageable pageable);

    List<WorkSchedule> findByStaffIdAndTypeAndDate(Long staffId, Short type, LocalDate date);

    @Query(value = "select * from #{#entityName} where type = 1 and date = ?1 and staff_id = ?2", nativeQuery = true)
    Optional<WorkSchedule> findHolidayScheduleByDate(LocalDate date, Long staffId);

    Optional<WorkSchedule> findByIdAndStaffId(Long id, Long staffId);

    @Query(value = "select * from #{#entityName} where type = ?1 and date = CURRENT_DATE and staff_id = ?2", nativeQuery = true)
    List<WorkSchedule> findTodayDoctorSchedule(Short type, Long staffId);

    @Query(value = "select * from #{#entityName} where type = 2 and time_slot_id_end <= 32 and date = ?1 and staff_id = ?2", nativeQuery = true)
    Optional<WorkSchedule> findMorningOverTimeScheduleBy(LocalDate date, Long staffId);

    @Query(value = "select * from #{#entityName} where type = 2 and time_slot_id_end >= 73 and date = ?1 and staff_id = ?2", nativeQuery = true)
    Optional<WorkSchedule> findNightOverTimeScheduleBy(LocalDate date, Long staffId);

    @Query(value = "select date, staff_id, " +
            "max(case when seqnum = 1 then time_slot_id_start end) as time_slot_id_start_1, " +
            "max(case when seqnum = 1 then time_slot_id_end end) as time_slot_id_end_1, " +
            "max(case when seqnum = 2 then time_slot_id_start end) as time_slot_id_start_2, " +
            "max(case when seqnum = 2 then time_slot_id_end end) as time_slot_id_end_2 " +
            "from " +
            "(select date, staff_id, time_slot_id_start, time_slot_id_end, " +
            "row_number() over (partition by date, staff_id order by time_slot_id_start, time_slot_id_end) as seqnum " +
            "from tbl_work_schedule where type = 2 and staff_id = :staffId) tbl " +
            "group by date, staff_id order by date desc", nativeQuery = true)
    Page<IOvertimeSchedule> searchDoctorOvertimeSchedule(@Param("staffId") Long staffId, Pageable pageable);

    @Query(value = "select date, staff_id, " +
            "max(case when seqnum = 1 then time_slot_id_start end) as time_slot_id_start_1, " +
            "max(case when seqnum = 1 then time_slot_id_end end) as time_slot_id_end_1, " +
            "max(case when seqnum = 2 then time_slot_id_start end) as time_slot_id_start_2, " +
            "max(case when seqnum = 2 then time_slot_id_end end) as time_slot_id_end_2 " +
            "from " +
            "(select date, staff_id, time_slot_id_start, time_slot_id_end, " +
            "row_number() over (partition by date, staff_id order by time_slot_id_start, time_slot_id_end) as seqnum " +
            "from tbl_work_schedule where type = 2 and staff_id = :staffId and date = :date) tbl " +
            "group by date, staff_id order by date desc", nativeQuery = true)
    Page<IOvertimeSchedule> searchDoctorOvertimeScheduleByDate(@Param("date") LocalDate date, @Param("staffId") Long staffId, Pageable pageable);

    @Query(value = "select date, staff_id, " +
            "max(case when seqnum = 1 then time_slot_id_start end) as time_slot_id_start_1, " +
            "max(case when seqnum = 1 then time_slot_id_end end) as time_slot_id_end_1, " +
            "max(case when seqnum = 2 then time_slot_id_start end) as time_slot_id_start_2, " +
            "max(case when seqnum = 2 then time_slot_id_end end) as time_slot_id_end_2 " +
            "from " +
            "(select date, staff_id, time_slot_id_start, time_slot_id_end, " +
            "row_number() over (partition by date, staff_id order by time_slot_id_start, time_slot_id_end) as seqnum " +
            "from tbl_work_schedule where type = 2 and staff_id = :staffId and date = CURRENT_DATE) tbl " +
            "group by date, staff_id order by date desc", nativeQuery = true)
    List<IOvertimeSchedule> findTodayDoctorOvertimeSchedule(@Param("staffId") Long staffId);

    @Query(value = "select date, staff_id, " +
            "max(case when seqnum = 1 then time_slot_id_start end) as time_slot_id_start_1, " +
            "max(case when seqnum = 1 then time_slot_id_end end) as time_slot_id_end_1, " +
            "max(case when seqnum = 2 then time_slot_id_start end) as time_slot_id_start_2, " +
            "max(case when seqnum = 2 then time_slot_id_end end) as time_slot_id_end_2 " +
            "from " +
            "(select date, staff_id, time_slot_id_start, time_slot_id_end, " +
            "row_number() over (partition by date, staff_id order by time_slot_id_start, time_slot_id_end) as seqnum " +
            "from tbl_work_schedule where type = 2) tbl " +
            "group by date, staff_id order by date desc", nativeQuery = true)
    Page<IOvertimeSchedule> searchOvertimeScheduleForAdmin(Pageable pageable);

    @Query(value = "select date, staff_id, " +
            "max(case when seqnum = 1 then time_slot_id_start end) as time_slot_id_start_1, " +
            "max(case when seqnum = 1 then time_slot_id_end end) as time_slot_id_end_1, " +
            "max(case when seqnum = 2 then time_slot_id_start end) as time_slot_id_start_2, " +
            "max(case when seqnum = 2 then time_slot_id_end end) as time_slot_id_end_2 " +
            "from " +
            "(select date, staff_id, time_slot_id_start, time_slot_id_end, " +
            "row_number() over (partition by date, staff_id order by time_slot_id_start, time_slot_id_end) as seqnum " +
            "from tbl_work_schedule where type = 2 and date = :date) tbl " +
            "group by date, staff_id order by date desc", nativeQuery = true)
    Page<IOvertimeSchedule> searchOvertimeScheduleByDateForAdmin(@Param("date") LocalDate date,Pageable pageable);
}

