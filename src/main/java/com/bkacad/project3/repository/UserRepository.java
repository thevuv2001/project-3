package com.bkacad.project3.repository;

import com.bkacad.project3.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    Optional<User> findByUsernameAndIsLock(String username, Boolean isLock);

    List<User> findByRoleOrderByCreatedAtDesc(Short role);

    Optional<User> findByUsername(String username);

    Optional<User> findByIdAndIsLock(Long id, Boolean isLock);

    @Query(value = "select t.* from tbl_user t where t.role = :role and " +
            "(" +
            "lower(REPLACE(t.username,' ','')) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) " +
            "or lower(REPLACE(t.full_name,' ','')) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) " +
            ") order by t.created_at desc", nativeQuery = true)
    Page<User> searchByRole(@Param("role") Short role, @Param("search") String search, Pageable pageable);

    @Query(value = "select t.* from tbl_user t where t.role = 2 and t.specialty_id is null order by t.created_at desc", nativeQuery = true)
    List<User> findDoctorDontHaveSpecialty();

    Long countBySpecialtyId(Long specialtyId);

    List<User> findBySpecialtyIdOrderByCreatedAtDesc(Long id);

    @Query(value = "select count(t) from tbl_user t where t.role in ?1 and date_part('year', t.created_at) = ?2", nativeQuery = true)
    Long countByRoleInAndYear(List<Short> roleIdList, Integer year);

    @Query(value = "select count(t) from tbl_user t where date_part('year', t.created_at) = ?1 and date_part('month', t.created_at) = ?2", nativeQuery = true)
    Long countByYearAndMonth(Integer year, Integer month);

    Long countByRoleInAndIsLockFalse(List<Short> roleList);

    List<User> findBySpecialtyId(Long id);
}
