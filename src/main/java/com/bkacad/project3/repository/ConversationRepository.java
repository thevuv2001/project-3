package com.bkacad.project3.repository;

import com.bkacad.project3.domain.Conversation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Long>, JpaSpecificationExecutor<Conversation> {
    Optional<Conversation> findByCustomerId(Long id);

    @Query("select t from tbl_conversation t where t.supporterId = ?1 and t.lastMessage is not null")
    Page<Conversation> findBySupporterId(Long id, Pageable pageable);
}
