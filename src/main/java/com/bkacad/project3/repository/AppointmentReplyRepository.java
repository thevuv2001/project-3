package com.bkacad.project3.repository;

import com.bkacad.project3.domain.AppointmentReply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentReplyRepository extends JpaRepository<AppointmentReply, Long>, JpaSpecificationExecutor<AppointmentReply> {
    List<AppointmentReply> findByAppointmentId(Long id);

    List<AppointmentReply> findByAdminId(Long id);
}
