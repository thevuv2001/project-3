package com.bkacad.project3.repository;

import com.bkacad.project3.domain.AppointmentSchedule;
import com.bkacad.project3.dto.ICustomerAppointmentRank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentScheduleRepository extends JpaRepository<AppointmentSchedule, Long>, JpaSpecificationExecutor<AppointmentSchedule> {
    Optional<AppointmentSchedule> findByIdAndCustomerId(Long id, Long customerId);

    Optional<AppointmentSchedule> findByIdAndDoctorId(Long id, Long doctorId);

    Page<AppointmentSchedule> findByCustomerId(Long id, Pageable pageable);

    Page<AppointmentSchedule> findByCustomerIdAndStatusIn(Long id, List<Short> statusList, Pageable pageable);

    Page<AppointmentSchedule> findByStatusIn(List<Short> statusList, Pageable pageable);

    Page<AppointmentSchedule> findByDoctorIdAndStatusIn(Long id, List<Short> statusList, Pageable pageable);

    Page<AppointmentSchedule> findByDoctorIdAndStatusInAndDate(Long id, List<Short> statusList, LocalDate date, Pageable pageable);

    Long countAllByCustomerId(Long id);

    Long countByCustomerIdAndStatusIn(Long id, List<Short> status);

    Long countByCustomerIdAndStatus(Long id, Short status);

    Long countByStatusIn(List<Short> status);

    Long countByStatus(Short status);

    List<AppointmentSchedule> findByDoctorIdAndStatusIn(Long doctorId, List<Short> status);

    List<AppointmentSchedule> findByDoctorIdAndStatusInAndDate(Long doctorId, List<Short> status, LocalDate date);

    List<AppointmentSchedule> findByDoctorIdAndStatusInAndDateAndTimeSlotIdInAndIdNot(Long doctorId, List<Short> status, LocalDate date, List<Long> timeSlotId, Long id);

    Long countByDoctorIdAndTimeSlotIdAndDateAndStatusInAndIdNot(Long doctorId, Long timeSlotId, LocalDate date, List<Short> status, Long id);

    @Query(value = "select count(t) from tbl_appointment_schedule t where t.status in ?1 and date_part('year', t.date) = ?2", nativeQuery = true)
    Long countByStatusInAndYear(List<Short> status, Integer year);

    @Query(value = "select count(t) from tbl_appointment_schedule t where t.status in ?1 and date_part('year', t.date) = ?2 and date_part('month', t.date) = ?3", nativeQuery = true)
    Long countByStatusInAndYearAndMonth(List<Short> statusList, Integer year, Integer month);

    @Query(value = "select count(t) from tbl_appointment_schedule t where t.status in ?1 and date_part('year', t.date) = ?2 and date_part('month', t.date) = ?3  and date_part('day', t.date) = ?4", nativeQuery = true)
    Long countByStatusInAndYearAndMonthAndDay(List<Short> statusList, Integer year, Integer month, Integer day);

    @Query(value = "select * from (select u.id, u.full_name, count(a.*) as count from tbl_user u " +
            "left join tbl_appointment_schedule a on u.id = a.customer_id " +
            "where a.status > 4 and u.role = 4 group by u.id limit 10 ) tbl order by count desc", nativeQuery = true)
    List<ICustomerAppointmentRank> customerRanking();

    @Query(value = "select * from (select u.id, u.full_name, count(a.*) as count from tbl_user u " +
            "inner join tbl_appointment_schedule a on u.id = a.doctor_id " +
            "where a.status > 5 and u.role = 2 group by u.id limit 10 ) tbl order by count desc", nativeQuery = true)
    List<ICustomerAppointmentRank> doctorRanking();

    Long countByDoctorIdInAndStatusIn(List<Long> doctorIdList, List<Short> statusList);
}
