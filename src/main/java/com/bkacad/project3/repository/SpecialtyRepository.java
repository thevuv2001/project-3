package com.bkacad.project3.repository;

import com.bkacad.project3.domain.Specialty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialtyRepository extends JpaRepository<Specialty, Long>, JpaSpecificationExecutor<Specialty> {
    @Query(value = "select t.* from tbl_specialty t where lower(REPLACE(t.name,' ','')) like lower(CONCAT('%',(REPLACE(:search,' ','')),'%')) order by t.created_at desc", nativeQuery = true)
    Page<Specialty> search(@Param("search") String search, Pageable pageable);

    @Query(value = "select t.* from tbl_specialty t where lower(REPLACE(t.name,' ','')) = lower(REPLACE(:name,' ',''))", nativeQuery = true)
    Optional<Specialty> findByName(@Param("name") String name);

    @Query(value = "select count(t) from tbl_specialty t where date_part('year', t.created_at) = ?1", nativeQuery = true)
    Long countByYear(Integer year);
}
