package com.bkacad.project3.repository;

import com.bkacad.project3.domain.NotificationToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationTokenRepository extends JpaRepository<NotificationToken, Long>, JpaSpecificationExecutor<NotificationToken> {
}
