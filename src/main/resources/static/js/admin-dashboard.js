// All javascript code in this project for now is just for demo DON'T RELY ON IT

const random = (max = maxStaffCount) => {
  return Math.round(Math.random() * max);
}

const randomData = () => {
  return [
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
    random(),
  ]
}

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

const cssColors = (color) => {
  return getComputedStyle(document.documentElement).getPropertyValue(color)
}

const getColor = () => {
  return window.localStorage.getItem('color') ?? 'cyan'
}

const colors = {
  primary: cssColors(`--color-${getColor()}`),
  primaryLight: cssColors(`--color-${getColor()}-light`),
  primaryLighter: cssColors(`--color-${getColor()}-lighter`),
  primaryDark: cssColors(`--color-${getColor()}-dark`),
  primaryDarker: cssColors(`--color-${getColor()}-darker`),
}

const barChart = new Chart(document.getElementById('barChart'), {
  type: 'bar',
  data: {
    labels: months,
    datasets: [
      {
        data: yearNewUserChart,
        backgroundColor: '#33ccff',
        hoverBackgroundColor: '#00bfff',
      },
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          gridLines: false,
          ticks: {
            beginAtZero: true,
            stepSize: 50,
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 10,
          },
        },
      ],
      xAxes: [
        {
          gridLines: false,
          ticks: {
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 5,
          },
          categoryPercentage: 0.5,
          maxBarThickness: '50',
        },
      ],
    },
    cornerRadius: 2,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
  },
})

const doughnutChart = new Chart(document.getElementById('doughnutChart'), {
  type: 'doughnut',
  data: {
    labels: specialtyChartTitle,
    datasets: [
      {
        data: specialtyChartValue,
        backgroundColor: ['#99e6ff', '#66d9ff', '#33ccff', '#00bfff', '#0099cc'],
        hoverBackgroundColor: colors.primaryDark,
        borderWidth: 0,
        weight: 0.5,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom',
    },

    title: {
      display: false,
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
  },
})

const activeUsersChart = new Chart(document.getElementById('activeUsersChart'), {
  type: 'bar',
  data: {
    labels: [...randomData(), ...randomData()],
    datasets: [
      {
        data: [...randomData(), ...randomData()],
        backgroundColor: '#33ccff',
        borderWidth: 0,
        categoryPercentage: 1,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          display: false,
          gridLines: false,
        },
      ],
      xAxes: [
        {
          display: false,
          gridLines: false,
        },
      ],
      ticks: {
        padding: 10,
      },
    },
    cornerRadius: 2,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      prefix: 'Users',
      bodySpacing: 4,
      footerSpacing: 4,
      hasIndicator: true,
      mode: 'index',
      intersect: true,
    },
    hover: {
      mode: 'nearest',
      intersect: true,
    },
  },
})

const lineChart = new Chart(document.getElementById('lineChart'), {
  type: 'line',
  data: {
    labels: months,
    datasets: [
      {
        label: 'Tất cả',
        data: yearAllAppointmentChart,
        fill: false,
        borderColor: '#33ccff',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Hoạt động',
        data: yearActiveAppointmentChart,
        fill: false,
        borderColor: '#33cc33',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Hoàn thành',
        data: yearCompleteAppointmentChart,
        fill: false,
        borderColor: '#ff9900',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Từ chối',
        data: yearRejectAppointmentChart,
        fill: false,
        borderColor: '#ff0000',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
    ],
  },
  options: {
    responsive: true,
    scales: {
      yAxes: [
        {
          gridLines: false,
          ticks: {
            beginAtZero: false,
            stepSize: 50,
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 20,
          },
        },
      ],
      xAxes: [
        {
          gridLines: false,
        },
      ],
    },
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      hasIndicator: true,
      intersect: false,
    },
  },
})

const appointmentChart = new Chart(document.getElementById('appointmentChart'), {
  type: 'line',
  data: {
    labels: dayList,
    datasets: [
      {
        label: 'Tất cả',
        data: monthAllAppointmentChart,
        fill: false,
        borderColor: '#33ccff',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Hoạt động',
        data: monthActiveAppointmentChart,
        fill: false,
        borderColor: '#33cc33',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Hoàn thành',
        data: monthCompleteAppointmentChart,
        fill: false,
        borderColor: '#ff9900',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
      {
        label: 'Từ chối',
        data: monthRejectAppointmentChart,
        fill: false,
        borderColor: '#ff0000',
        borderWidth: 2,
        pointRadius: 0,
        pointHoverRadius: 0,
      },
    ],
  },
  options: {
    responsive: true,
    scales: {
      yAxes: [
        {
          gridLines: false,
          ticks: {
            beginAtZero: false,
            stepSize: 50,
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 20,
          },
        },
      ],
      xAxes: [
        {
          gridLines: false,
        },
      ],
    },
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
    tooltips: {
      hasIndicator: true,
      intersect: false,
    },
  },
})

const statusAppointmentChart = new Chart(document.getElementById('statusChart'), {
  type: 'doughnut',
  data: {
    labels: ['Đang chờ', 'Trả về', 'Hoạt động', 'Từ chối', 'Hoàn thành'],
    datasets: [
      {
        data: statusChart,
        backgroundColor: ['#99e6ff', '#66d9ff', '#33ccff', '#00bfff', '#0099cc'],
        hoverBackgroundColor: colors.primaryDark,
        borderWidth: 0,
        weight: 0.5,
      },
    ],
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom',
    },

    title: {
      display: false,
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
  },
})

const doctorAppointmentRankingChart = new Chart(document.getElementById('doctorRankingChart'), {
  type: 'bar',
  data: {
    labels: doctorRankingChartTitle,
    datasets: [
      {
        data: doctorRankingChartValue,
        backgroundColor: '#33ccff',
        hoverBackgroundColor: '#00bfff',
      },
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          gridLines: false,
          ticks: {
            beginAtZero: true,
            stepSize: 50,
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 10,
          },
        },
      ],
      xAxes: [
        {
          gridLines: false,
          ticks: {
            fontSize: 12,
            fontColor: '#97a4af',
            fontFamily: 'Open Sans, sans-serif',
            padding: 5,
          },
          categoryPercentage: 0.5,
          maxBarThickness: '50',
        },
      ],
    },
    cornerRadius: 2,
    maintainAspectRatio: false,
    legend: {
      display: false,
    },
  },
})

let randomUserCount = 1

const usersCount = document.getElementById('usersCount')

const fakeUsersCount = () => {
  randomUserCount = random()
  activeUsersChart.data.datasets[0].data.push(randomUserCount)
  activeUsersChart.data.datasets[0].data.splice(0, 1)
  activeUsersChart.update()
  usersCount.innerText = randomUserCount
}

setInterval(() => {
  fakeUsersCount()
}, 1000)
