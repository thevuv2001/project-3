var connectingElement = $('.connecting');
var messageInput = $("#msg-input");

$(document).ready(function (){
    connect();
    $('#chat-content').animate({scrollTop: $('#chat-content').get(0).scrollHeight}, 0);
    $('#send').click(function () {
        var messageContent = messageInput.val().trim();

        if(messageContent && stompClient) {
            var chatMessage = {
                content: messageContent,
                senderId: userId
            };
            stompClient.send("/app/chat/conversation/"+topicId, {}, JSON.stringify(chatMessage));
            messageInput.val('');

        }
    });
});
function connect() {

    var socket = new SockJS('/stomp-endpoint');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, onConnected, onError);
    // event.preventDefault();
}

function onConnected() {
    // Subscribe to the Public Topic
    subscription = stompClient.subscribe('/topic/conversation/'+topicId, onMessageReceived);

    // Tell your username to the server
    // stompClient.send("/app/chat/internship/"+topicId,
    //     {},
    //     JSON.stringify({type: 'REGISTER'})
    // )

    connectingElement.hide();
}

function onError(error) {
    connectingElement.text('Không thể kết nối đến máy chủ. Vui lòng thử lại sau!');
    connectingElement.css('color','red');
}

function onMessageReceived(payload){
    var message = JSON.parse(payload.body);
    if(payload != null){
        if(message.senderId != userId){
            var chat =
                "<li class='flex justify-start'>" +
                "<div class='relative max-w-xl px-4 py-2 text-gray-700 rounded shadow'>" +
                "<span class='block'>"+ urlify(message.content) +"</span>" +
                "</div>" +
                "</li>";
        }else{
            var chat =
                "<li class='flex justify-end'>" +
                "<div class='relative max-w-xl px-4 py-2 text-gray-700 bg-gray-100 rounded shadow'>" +
                "<span class='block'>"+ urlify(message.content) +"</span>" +
                "</div>" +
                "</li>";
        }
        $('#message-list').append(chat);
    }
    $('#chat-content').animate({scrollTop: $('#chat-content').get(0).scrollHeight}, 0);
}

function urlify(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a class="underline text-blue-600 hover:text-blue-800 visited:text-purple-600" href="' + url + '">' + url + '</a>';
    })
}

messageInput.keypress(function (e) {
    if (e.keyCode == 13){
        if(e.shiftKey){
            // txtArea.value +=  '\r\n';
            messageInput.onkeydown(function (e){
                e.preventDefault();
            })
            return ;
        }else{
            $('#send').click();
        }
    }
    if (e.keyCode != 13){
        return;
    }
    return false;
});